import { StyleSheet, Platform } from 'react-native';
import { screen } from './common';
import { scale } from '../../utils/scaling';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    alignSelf: 'stretch',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: screen.header.headerHeight,
    padding: 0,
    alignSelf: 'stretch',
  },
  searchInputContainer: {
    flex: 1,
    borderRadius: scale(8),
    height: scale(32),
    paddingHorizontal: screen.padding.default,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleImage: {
    backgroundColor: 'transparent',
    height: screen.header.headerHeight * 0.5,
    resizeMode: 'contain',
  },
  titleText: {
    color: 'white',
    fontSize: screen.header.textSize,
    lineHeight: scale(22),
    fontFamily: 'sale-text-semibold',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  iconPlaceholder: {
    backgroundColor: 'transparent',
    height: '100%',
    width: scale(44),
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionPlaceholder: {
    backgroundColor: 'transparent',
    width: screen.header.headerHeight,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchHeader: {
    flexDirection: 'row',
    height: screen.searchBar.height,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBarInputStyle: {
    height: screen.searchBar.height - 16,
    backgroundColor: screen.backgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: screen.searchBar.fontSize,
  },
  titleButton: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scale(6),
  },
  titleButtonText: {
    color: 'white',
    fontFamily: 'sale-text-regular',
    fontSize: scale(15),
    lineHeight: scale(20),
    letterSpacing: -scale(0.2),
  },
});
