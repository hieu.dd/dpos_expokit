import { Util } from 'teko-js-sale-library';

import { flatBenefitFromPromotionWithQuantity } from '../modules/promotion';
import { LOCAL_VOUCHER_KEY } from '../constants';
import { getProductDetailBySku, isService } from './product';
import {
  getVoucherValueFromCart,
  calculateFinalCartInvoice,
  getCartWithImportPrice,
  getFinalCartInvoice,
  getProductsInCartToCheckRevenue,
} from './cart';
import { DEFAULT_CONFIG } from '../config/sale';
import { getOrderReview, checkOrderReviewPermission } from './user';
import * as customerUtils from '../utils/customer';

export function getInvoiceFromOrderlines(orderlines) {
  let price = 0;
  let totalPrice = 0;
  let discount = 0;
  for (let item of orderlines) {
    totalPrice += item.price * item.quantity;
    discount += item.discount_pv * item.quantity;
  }
  price = totalPrice - discount;

  return { price, totalPrice, discount };
}

export function makeOrderLinesBEToOrderLinesOm(orderlines) {
  let items = orderlines.map(item => {
    return {
      quantity: item.quantity,
      sku: item.product,
      unitPrice: item.price,
      productType: 'Biz',
      location: '0901',
      vat: 0,
      discountAmount: item.discount_pv,
      commission: 0,
      discountReason: 'GG000008',
    };
  });
  return items;
}

export function addServiceFee(orderlines, cart, customerInfo, productDetails) {
  let { technical_support, require_vat } = customerInfo;
  let finalInvoice = calculateFinalCartInvoice(cart);

  let feeSku = technical_support ? '1702079' : '1206838';
  // let serviceDetail = getProductDetailBySku(feeSku, productDetails);
  let serviceDetail = null;

  let servicePrice = 0;
  // calculate service fee, free if customer is a company (has tax)
  // if (require_vat) {
  //   servicePrice = 0;
  // } else {
  //   let deliverFee = getDeliverFee(finalInvoice);
  //   let technicalSupportFee = technical_support ? getTechnicalSupportFee(finalInvoice) : 0;
  //   servicePrice = deliverFee + technicalSupportFee;
  // }

  orderlines = [
    ...orderlines,
    {
      name: serviceDetail.name,
      price: servicePrice,
      bundle_meta: null,
      description: '',
      discount_item: 0,
      discount_pv: 0,
      product: String(feeSku),
      quantity: 1,
      is_gift: true,
      sku: feeSku,
    },
  ];

  return orderlines;
}

function getDeliverFee(invoice) {
  return invoice >= 500000 ? 0 : 11000;
}

function getTechnicalSupportFee(invoice) {
  return invoice >= 5000000 ? 0 : 55000;
}

export function addDeliverFee(orderlines) {
  //add deliver fee
  let sum = 0;
  for (let product of orderlines) {
    if (product.discount >= 0 && product.price >= 0) {
      sum += (product.price - product.discount) * product.quantity;
    }
  }
  let deliverFee = sum >= 500000 ? 0 : 11000;
  orderlines = [
    ...orderlines,
    {
      name: 'Phí dịch vụ giao hàng tận nơi',
      price: deliverFee,
      bundle_meta: null,
      description: '',
      discount: 0,
      product: 1206838,
      quantity: 1,
      sku: '1206838',
      source_url: null,
    },
  ];

  return orderlines;
}

export function getVoucherDataInCart(cart) {
  let voucherData = {};
  const { voucher } = cart;
  if (voucher) {
    if (voucher.code) {
      voucherData.voucher_code = voucher.code;
    }

    voucherData = {
      ...voucherData,
      promotion_key: voucher.key && voucher.key !== LOCAL_VOUCHER_KEY ? voucher.key : null,
      voucher_key_fb: voucher.hasUseLater ? voucher.key : null,
      send_vou_to_cus: voucher.hasUseLater || false,
      voucher_value: voucher.hasUseLater ? 0 : getVoucherValueFromCart(cart),
      enterpriseType: voucher.enterpriseType ? voucher.enterpriseType : [],
    };
  }

  let flattedBenefitsInCart = getAllFlattedBenefitsInCart(cart);
  let voucherBenefits = flattedBenefitsInCart.filter(item => item.voucher);
  let useLaterVoucherKeysString = getUseLaterVoucherKeysString(voucherBenefits);
  if (useLaterVoucherKeysString) {
    voucherData = {
      ...voucherData,
      send_vou_to_cus: true,
      voucher_key_fb: concatStrings(',', voucherData.voucher_key_fb, useLaterVoucherKeysString),
    };
  }

  return voucherData;
}

export function getMultiplyString(str, n) {
  return str + (n > 1 ? `*${n}` : '');
}

export function concatStrings(delimiter, ...strings) {
  return strings.filter(str => str).join(delimiter);
}

export function getUseLaterVoucherKeysString(benefits) {
  let voucherKeysString = '';
  let useLaterVouchers = {};
  for (let benefit of benefits) {
    if (benefit.voucher && benefit.voucher.use_later && benefit.quantity > 0) {
      let voucherQuantity = benefit.quantity * (benefit.voucher.quantity || 1);

      if (useLaterVouchers[benefit.voucher.key]) {
        useLaterVouchers[benefit.voucher.key] += voucherQuantity;
      } else {
        useLaterVouchers[benefit.voucher.key] = voucherQuantity;
      }
    }
  }

  voucherKeysString = Object.keys(useLaterVouchers)
    .map(key => getMultiplyString(key, useLaterVouchers[key]))
    .join();

  return voucherKeysString;
}

export function getAllFlattedBenefitsInCart(cart) {
  const { items, combos } = cart;
  let flattedBenefits = [];

  for (let item of items) {
    flattedBenefits = flattedBenefits.concat(flatBenefitFromPromotionWithQuantity(item.promotion, item.quantity));
  }

  for (let combo of combos) {
    flattedBenefits = flattedBenefits.concat(flatBenefitFromPromotionWithQuantity(combo.promotion, combo.quantity));
  }

  return flattedBenefits;
}

export function getDepositFromCustomerInfo(customerInfo) {
  return !customerInfo.showDeposit
    ? null
    : {
        amount: parseInt(customerInfo.deposit.cash),
        method: customerInfo.deposit.method,
      };
}

export function getCustomerFromCustomerInfo(customerInfo) {
  let { id, name, shipping, phone, asia_crm_id } = customerInfo;
  let address = (shipping && customerUtils.getFullAddressOfShipping(shipping)) || '';
  return { id, name, phone, address, asiaCrmId: asia_crm_id };
}

export function getShippingFromCustomerInfo(customerInfo) {
  return customerInfo.shipping;
}

export function getShippingInfoFromCustomerInfo(customerInfo) {
  let { name, shipping, phone } = customerInfo;
  let address = customerUtils.getFullAddressOfShipping(shipping);
  return {
    name,
    address,
    street: shipping.street,
    email: '',
    telephone: phone,
    addressCode: shipping.commune_code,
  };
}

export function convertOrdersToLocal(orders) {
  return orders.map(order => {
    if (getRealOrderType(order) === 'order') {
      return mapOrderToLocal(order);
    } else {
      return mapQuotationToLocal(order);
    }
  });
}

export function isQuotation(rawOrder) {
  return rawOrder.status === 'draft';
}

export function getRealOrderType(order) {
  return isQuotation(order) ? 'quotation' : 'order';
}

export function mapOrderToLocal(entry) {
  if (getRealOrderType(entry) === 'quotation') {
    return mapQuotationToLocal(entry);
  }

  let { items } = convertOrderlines(entry.orderlines);
  let invoice = {
    total: parseInt(entry.price_total_w_pd) + parseInt(entry.total_discount_w_pd),
    final: parseInt(entry.price_total_w_pd),
    serviceFee: calculateServiceFee(entry.orderlines),
    grand_discount: parseInt(entry.discount_order), // giảm giá toàn đơn xin thêm
    voucher_value: parseInt(entry.voucher_value),
    voucher_code: entry.voucher_code,
    price: parseInt(entry.price_total),
    deposit: parseInt(entry.deposit) || 0,
    deposit_method: entry.deposit_method,
  };

  return {
    agent_province: entry.agent_province,
    magento_id: entry.entity_id,
    id: entry.id,
    invoice,
    commission: getOrderCommission(entry.expected_commission, invoice.final - invoice.serviceFee),
    order_date: entry.order_date ? new Date(entry.order_date).toISOString() : null,
    created_date: new Date(entry.created).toISOString(),
    exported_at: entry.exported_at * 1000,
    ship_date: entry.ship_date,
    billing_info: {
      name: entry.billing_name,
      email: entry.billing_email,
      phone: entry.billing_phone,
      taxcode: entry.billing_taxcode,
      street: entry.billing_street,
      district: entry.billing_district,
      // province: utils.getProvinceFromDistrictCode(entry.billing_district).name
    },
    shipping_info: {
      name: entry.shipping_name,
      email: entry.shipping_email,
      phone: entry.shipping_phone,
      street: entry.shipping_street,
      district: entry.shipping_district,
      // province: utils.getProvinceFromDistrictCode(entry.shipping_district).name
    },
    customer: getOrderCustomer(entry),
    status: entry.status,
    status_discount: entry.status_discount,
    saleman: entry.saleman,
    items: items,
    note: entry.note,
    expected_profit: entry.expected_profit,
    manager_note: entry.manager_note,
    extra_info: entry.extra_info,
    parent_id: entry.parent_id,
  };
}

function convertOrderlines(orderlines) {
  let items = orderlines.map(makeCartItemFromOrderline);

  return { items };
}

function makeCartItemFromOrderline(orderline) {
  return {
    product: {
      id: orderline.product,
      price: parseInt(orderline.price),
      discount_pv: parseInt(orderline.discount_pv),
      discount_item: parseInt(orderline.discount_item),
      discount_reason: orderline.bundle_meta,
      image: orderline.product_image,
      sku: orderline.product,
      name: orderline.product_name,
    },
    id: orderline.id,
    quantity: orderline.quantity,
    delivered_quantity: orderline.delivered_quantity,
    returned_quantity: orderline.returned_quantity,
  };
}

export function convertQuotationsToLocal(orders) {
  return orders.map(mapOrderToLocal);
}

export function mapQuotationToLocal(entry) {
  let { items } = convertOrderlines(entry.orderlines);
  let expired_date = '';
  let state = '';
  let stateDateDiscount = 'draft';
  if (entry.expiration_date === null) {
    state = 'sale';
    stateDateDiscount = 'draft';
  } else {
    state = checkExpiredDate(entry.expiration_date) ? 'expired' : 'pending';
    stateDateDiscount =
      checkExpiredDate(entry.expiration_date_discount) && entry.status_discount === 'pending' ? 'expired' : 'draft';
    expired_date = getExpirationTime(entry.expiration_date).toISOString();
  }

  if (state === 'pending' && entry.status_discount !== 'pending') {
    state = entry.status_discount || entry.status;
  }

  if (stateDateDiscount === 'expired') {
    state = 'expired_order_propose';
  }

  let invoice = {
    total: parseInt(entry.price_total_w_pd) + parseInt(entry.total_discount_w_pd),
    final: parseInt(entry.price_total_w_pd),
    serviceFee: calculateServiceFee(entry.orderlines),
    grand_discount: parseInt(entry.discount_order), // giảm giá toàn đơn xin thêm
    voucher_value: parseInt(entry.voucher_value),
    voucher_code: entry.voucher_code,
    price: parseInt(entry.price_total),
    deposit: parseInt(entry.deposit) || 0,
    deposit_method: entry.deposit_method,
  };

  return {
    agent_province: entry.agent_province,
    stateDateDiscount: stateDateDiscount,
    id: entry.id,
    status: state,
    status_discount: entry.status_discount,
    invoice,
    commission: getOrderCommission(entry.expected_commission, invoice.final - invoice.serviceFee),
    saleInvoice: {
      total: parseInt(entry.price_total) + parseInt(entry.discount),
      final: parseInt(entry.price_total_w_pd),
      grand_discount: parseInt(entry.discount_order), // giảm giá toàn đơn xin thêm
      price: parseInt(entry.price_total),
      deposit: parseInt(entry.deposit) || 0,
    },
    created_date: new Date(entry.created).toISOString(),
    ship_date: entry.ship_date,
    expired_date: expired_date,
    exported_at: entry.exported_at * 1000,
    billing_info: {
      name: entry.billing_name,
      email: entry.billing_email,
      phone: entry.billing_phone,
      taxcode: entry.billing_taxcode,
      street: entry.billing_street,
      district: entry.billing_district,
      // province: utils.getProvinceFromDistrictCode(entry.billing_district).name
    },
    shipping_info: {
      name: entry.shipping_name,
      email: entry.shipping_email,
      phone: entry.shipping_phone,
      street: entry.shipping_street,
      district: entry.shipping_district,
    },
    customer: getOrderCustomer(entry),
    items: items,
    note: entry.note,
    proposed_discount: parseInt(entry.proposed_discount),
    discount_desc: entry.discount_desc,
    saleman: entry.saleman,
    manager_note: entry.manager_note,
    manager: entry.manager,
    expected_profit: entry.expected_profit,
    extra_info: entry.extra_info,
  };
}

function getOrderCustomer(order) {
  return {
    id: order.customer_id,
    GcafeId: order.gcafe_id,
    asia_crm_id: order.asia_crm_id,
    name: order.customer_name,
    phone: order.customer_phone,

    // hiện chưa có
    // email: entry.customer_email,
    // street: entry.customer.street,
    // district: entry.customer.district,
    // province: entry.customer.province
  };
}

export function getOrderCommission(expectedCommission, invoice) {
  let value = expectedCommission ? Math.round(expectedCommission) : 0;
  let percentage = ((value / invoice) * 100).toFixed(2);

  return {
    value,
    percentage,
    amountToCompute: invoice,
  };
}

export function calculateServiceFee(orderlines) {
  return orderlines.reduce((serviceFee, item) => serviceFee + (isService(String(item.product)) ? parseInt(item.price) : 0), 0);
}

export function checkExpiredDate(expirationDate) {
  let expirationTime = getExpirationTime(expirationDate);
  return expirationTime.getTime() < Date.now();
}

function getExpirationTime(expirationDate) {
  let expirationTime = new Date(expirationDate + 'T16:59:00Z');
  expirationTime.setDate(expirationTime.getDate() - 1);

  return expirationTime;
}

export function getOrderPageFromState(saleState) {
  return saleState.nextOrderPage;
}

export function getQuotationPageFromState(saleState) {
  return saleState.nextQuotationPage;
}

export function computeCommissionForValue(value, config) {
  if (!config) {
    return {
      amountToCompute: 0,
      percentage: 0,
      value: 0,
    };
  }

  let commissionPercentage = getCommissionPercentage(value, config.commission);
  let adjustRate = config.factor || 0.9 / 1.1; // VAT filter & tax prehold
  let realPercentage = commissionPercentage * adjustRate;
  let commissionValue = Math.round((value * realPercentage) / 100);
  realPercentage = realPercentage.toFixed(2);

  return {
    amountToCompute: value,
    percentage: realPercentage,
    value: commissionValue,
  };
}

export function getCommissionPercentage(amount, commissionConfig) {
  for (let config of commissionConfig) {
    let { max, min } = config.range;

    if (!max) {
      if (amount > min.value || (min.inclusive && min.value === amount)) {
        return config.percentage;
      }
    } else if (!min) {
      if (amount < max.value || (max.inclusive && max.value === amount)) {
        return config.percentage;
      }
    } else if (
      (amount > min.value && amount < max.value) ||
      (min.inclusive && min.value === amount) ||
      (max.inclusive && max.value === amount)
    ) {
      return config.percentage;
    }
  }

  return 0;
}

export function getOrderParamsScope(user) {
  let reviewOrderPermissions = getOrderReview(user);
  let provinces = '';
  let saleman = '';
  if (reviewOrderPermissions) {
    saleman = 'all';
    provinces = reviewOrderPermissions.provinces ? reviewOrderPermissions.provinces.join() : '';
  }

  return {
    saleman,
    provinces,
  };
}

export function getOrderAction(order, user) {
  if (!order || !user) return null;
  let action = 'none';
  let hasOrderReviewPermission = checkOrderReviewPermission(order, user);
  let isSaleman = checkSaleman(order, user);

  if (isSaleman || hasOrderReviewPermission) {
    if (order.stateDateDiscount === 'expired') {
      action = 'expired_order_propose';
    } else if (order.status === 'expired') {
      action = 'expired_date';
    } else if (order.status === 'pending') {
      action = hasOrderReviewPermission ? 'order_review' : isSaleman ? 'view' : 'none';
    } else if (order.status === 'approved' || order.status === 'draft') {
      action = isSaleman ? 'update_order' : 'none';
    } else if (order.status === 'rejected') {
      action = isSaleman ? 'edit' : 'none';
    } else if (order.status === 'canceled') {
      action = isSaleman ? 'edit' : 'none';
    } else {
      action = isSaleman ? 'view' : 'none';
    }
  }

  return action;
}

export function checkSaleman(order, user) {
  let username = user && user.userInfo && user.userInfo.username;

  return order.saleman.indexOf(username) !== -1;
}

export function getMagentoMessage(magentoMsgStr) {
  let magentoMsg = JSON.parse(magentoMsgStr);

  return Util.Text.unicodeToChar(magentoMsg.msg);
}

export function calculateCartRevenue(cart, importPrices, walletConfig) {
  let cartWithImportPrice = getCartWithImportPrice(cart, importPrices);

  // hiện tại chưa có quà xin thêm nên đang bỏ qua
  let finalInvoice = getFinalCartInvoice(cartWithImportPrice);
  let productsInCart = getProductsInCartToCheckRevenue(cartWithImportPrice);
  let commission = computeCommissionForValue(finalInvoice, walletConfig);
  let totalImportPrice = getTotalImportPrice(productsInCart, importPrices);

  return ((finalInvoice - commission.value - totalImportPrice) / totalImportPrice) * 100;
}

export function getTotalImportPrice(products, importPrices) {
  let totalPrice = 0;
  for (let product of products) {
    let importPrice = importPrices[product.sku] && importPrices[product.sku].price;
    if (!importPrice && importPrice > 0) {
      continue;
    }
    totalPrice += importPrice * product.quantity;
  }

  return totalPrice;
}

export function shouldCheckProductStatus(config) {
  return config && config.sale && config.sale.should_check_product_status;
}

export function getSalemanName(saleman = '') {
  let partials = saleman.split('-');
  if (partials.length !== 2) {
    return saleman;
  } else {
    let name = partials[1].trim();

    return name !== '' ? name : saleman;
  }
}
