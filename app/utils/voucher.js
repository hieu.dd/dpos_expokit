import VoucherService from 'teko-js-sale-library/packages/Service/VoucherService';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import TRACK from '../config/TrackingPrototype';
import { getUserPromotionScope } from './user';
import { checkVoucher, isExpiredVoucher } from '../modules/promotion/voucher';

export async function verifyVoucherCode(voucherCode, invoice, user) {
  let message = null;
  let ok = true;

  try {
    let ret = await VoucherService.verifyVoucherCode(voucherCode);
    if (ret.ok && ret.data) {
      if (ret.data.is_valid) {
        let voucher = ret.data.data && ret.data.data.data;
        let userPromotionScope = getUserPromotionScope(user);

        if (!checkVoucher(voucher, invoice, userPromotionScope)) {
          let invalidReason = isExpiredVoucher(voucher) ? 'đã hết hạn' : 'không hợp lệ';
          message = `Mã giảm giá ${voucherCode} ${invalidReason}`;
        }
      } else {
        ok = false;
        message = ret.data.message || `Mã giảm giá ${voucherCode} không hợp lệ`;
      }
    } else {
      ok = false;
      message = 'Không thể xác thực được mã giảm giá';
    }
  } catch (error) {
    Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'verifyVoucherCode', error.message, error.stack);

    ok = false;
    message = error.message;
  }

  return { ok, message };
}
