export function getOrderReview(user) {
  return (user && user.userInfo && user.userInfo.user_permissions && user.userInfo.user_permissions.order_review) || false;
}

export function checkOrderReviewPermission(order, user) {
  if (!user || !order) return false;

  let orderReview = getOrderReview(user);
  if (orderReview) {
    if (!orderReview.provinces) {
      return true;
    } else {
      return orderReview.provinces.includes(order.agent_province);
    }
  }

  return false;
}

export function hasWalletPermission(user) {
  return user && user.userInfo && user.userInfo.user_permissions && user.userInfo.user_permissions.wallet;
}

export function getWalletConfig(user) {
  return getPermissionConfig(user, 'wallet');
}

export function getUserPromotionScope(user) {
  let promotionConfig = getPermissionConfig(user, 'promotion');

  return {
    storeId: null,
    channel: 'agent',
    group: (promotionConfig && promotionConfig.group) || 'customer',
  };
}

export function getPermissionConfig(user, permission) {
  return (
    user &&
    user.userInfo &&
    user.userInfo.user_permissions &&
    user.userInfo.user_permissions[permission] &&
    user.userInfo.user_permissions[permission].config
  );
}
