import firebase from 'firebase';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import ServerTime from './server-time';
import * as strings from '../resources/strings';
import TRACK from '../config/TrackingPrototype';
import * as promotionPathUtils from '../modules/promotion/path';

export function getFirebaseUpdates(cart) {
  let { items, combos, voucher } = cart;
  let firebaseUpdates = {};
  for (let item of items) {
    addFirebaseUpdatePath(firebaseUpdates, item.promotion, item.quantity);
  }
  for (let combo of combos) {
    addFirebaseUpdatePath(firebaseUpdates, combo.promotion, combo.quantity);
  }

  if (voucher && voucher.updatePath) {
    addFirebaseUpdatePath(firebaseUpdates, voucher, 1);
  }

  return firebaseUpdates;
}

export function addFirebaseUpdatePath(firebaseUpdates, promotion, quantity) {
  if (!promotion) return firebaseUpdates;

  const { programKey, key, updatePath } = promotion;

  if (updatePath) {
    if (firebaseUpdates.hasOwnProperty(programKey)) {
      if (firebaseUpdates[programKey].hasOwnProperty(key)) {
        firebaseUpdates[programKey][key].quantity += quantity;
      } else {
        firebaseUpdates[programKey][key] = {
          path: updatePath,
          quantity,
        };
      }
    } else {
      firebaseUpdates[programKey] = {
        [key]: {
          path: updatePath,
          quantity,
        },
      };
    }
  }

  return firebaseUpdates;
}

export async function updatePromotionQuantityAndVoucherData(currentCart, firebaseUpdates, createdOrderResult) {
  updatePromotionQuantity(firebaseUpdates, createdOrderResult);

  await applyAndUploadImageForVoucher(currentCart.voucher, createdOrderResult.id, currentCart.created_by);
}

export async function lockQuantityOnFirebase(firebaseUpdates) {
  let serverTimeNow = ServerTime.get();

  for (let promotionKey in firebaseUpdates) {
    for (let key in firebaseUpdates[promotionKey]) {
      let entry = firebaseUpdates[promotionKey][key];
      let currentPromotionQuantity = await getDataFromFirebase(entry.path);

      if (!isNaN(parseInt(currentPromotionQuantity))) {
        if (currentPromotionQuantity <= 0) {
          // if promotion quantity runs out, return error
          return { ok: false, data: strings.sale_out_of_stock };
        } else if (currentPromotionQuantity <= 5) {
          // if remaining quantity small enough, we need to lock them to prevent extra buy
          let ret = await lockPromotionQuantity(promotionKey, key, currentPromotionQuantity, entry.quantity, serverTimeNow);
          if (!ret.ok) {
            return ret;
          }
        }
      }
    }
  }

  return { ok: true };
}

export async function lockPromotionQuantity(promotionKey, key, currentQuantity, lockQuantity, serverTimeNow) {
  return new Promise((resolve, reject) => {
    let result = { ok: false, data: strings.sale_error_checking_quantity };

    runTransaction(
      `promotions-dpos/quantity_lock/${promotionKey}/${key}`,
      current => {
        let ret = [];
        if (current && Array.isArray(current)) {
          ret = current.filter(e => e.expire > serverTimeNow);
        }

        let currentLockQuantity = ret.reduce((acc, cur) => acc + cur.lock, 0);

        // if total locking quantity + quantity going to buy < remaining then allow create order
        if (currentLockQuantity + lockQuantity <= currentQuantity) {
          result = { ok: true };
          return [...ret, { current: currentQuantity, lock: lockQuantity, expire: serverTimeNow + 30000 }]; // lock for 30s
        } else {
          result = { ok: false, data: strings.sale_out_of_stock };
          return ret;
        }
      },
      (error, commited, value) => {
        if (!error && commited) {
          resolve(result);
        } else {
          resolve({ ok: false, data: strings.sale_error_checking_quantity });
        }
      }
    );
  });
}

export function getDataFromFirebase(path) {
  return firebase
    .database()
    .ref(path)
    .once('value')
    .then(snapshot => {
      return snapshot.val();
    })
    .catch(error => {
      return null;
    });
}

export async function updatePromotionQuantity(firebaseUpdates, createdOrderResult) {
  // decrease firebase promotion quantity here
  for (let promotionKey in firebaseUpdates) {
    for (let key in firebaseUpdates[promotionKey]) {
      let entry = firebaseUpdates[promotionKey][key];
      runDecreaseQuantityTransaction(promotionKey, key, entry.path, entry.quantity, createdOrderResult.id);
    }
  }
}

export function runDecreaseQuantityTransaction(promotionKey, key, path, quantity, order_id) {
  let currentValue = null;

  runTransaction(
    path,
    current => {
      if (isNaN(parseInt(current))) {
        return;
      } else {
        currentValue = current;
        return current - quantity;
      }
    },
    (error, commited, value) => {
      if (!error && commited && currentValue !== null) {
        firebase
          .database()
          .ref(`history/promotion_quantity/${promotionKey}/${key}/${currentValue}_${Date.now()}_${order_id}`)
          .set({
            path,
            quantity,
            value: value.val(),
          });
      }
    }
  );
}

export function runTransaction(refPath, transact, onCompleted) {
  firebase
    .database()
    .ref(refPath)
    .transaction(transact, onCompleted);
}

export async function applyAndUploadImageForVoucher(voucher, orderId, creator) {
  let uploadedUrl = null;

  if (voucher.studentId && voucher.imageUri) {
    uploadedUrl = await uploadImageAsync(voucher.imageUri, `student_id_cards/${voucher.key}`, `${voucher.studentId}.jpg`);
    setDataOnFirebase(`${voucher.appliedStudentPath}/${voucher.studentId.toLowerCase()}`, {
      orderId,
      studentId: voucher.studentId,
      studentIdLower: voucher.studentId.toLowerCase(),
      imageUrl: uploadedUrl,
      createdBy: creator,
      createdAt: Date.now(),
    });
  } else if (voucher.has_required_code && voucher.code && voucher.imageUri) {
    uploadedUrl =
      uploadedUrl || (await uploadImageAsync(voucher.imageUri, `student_id_cards/${voucher.key}`, `${voucher.code}.jpg`));
    let path = promotionPathUtils.getAppliedCodeVoucherPath(voucher);
    setDataOnFirebase(path, {
      orderId,
      voucher_code: voucher.code,
      voucher_key: voucher.key,
      imageUrl: uploadedUrl,
      createdBy: creator,
      createdAt: Date.now(),
    });
  }
}

export function setDataOnFirebase(ref, data) {
  firebase
    .database()
    .ref(ref)
    .set(data);
}

export function pushDataToFirebase(ref, data) {
  let newPostKey = firebase
    .database()
    .ref(ref)
    .push().key;
  let updates = {};
  updates[`${ref}/${newPostKey}`] = data;

  firebase
    .database()
    .ref()
    .update(updates);
}

export async function uploadImageAsync(uri, refPath, filenameInFirebase) {
  let snapshot;
  try {
    const response = await fetch(uri);
    const blob = await response.blob();

    const imageRef = firebase
      .storage()
      .ref(refPath)
      .child(filenameInFirebase);

    snapshot = await imageRef.put(blob);
  } catch (error) {
    Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'uploadImageAsync', error.message, error.stack);
  }
  return (snapshot && snapshot.downloadURL) || 'Error';
}
