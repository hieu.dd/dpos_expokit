import { Platform } from 'react-native';
import { Constants } from 'expo';

class DeviceInfo {
  constructor() {}

  getDeviceInfo () {
    return {
      platform: Platform.OS,
      device: {
        model: Platform.OS === 'ios' ? Constants.platform.ios.model : Constants.deviceName
      }
    }
  }
}

export default new DeviceInfo();