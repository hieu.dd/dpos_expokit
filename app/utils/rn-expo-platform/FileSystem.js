import Expo from 'expo';

class FileSystem {
  constructor() {}

  async getInfoAsync(filePath) {
    return Expo.FileSystem.getInfoAsync(filePath);
  }

  async readAsStringAsync(filePath) {
    return Expo.FileSystem.readAsStringAsync(filePath);
  }

  async writeAsStringAsync(filePath, stringData) {
    return Expo.FileSystem.writeAsStringAsync(filePath, stringData);
  }

  getFolderPath(type) {
    switch (type) {
      case 'document':
        return Expo.FileSystem.documentDirectory;
      case 'cache':
        return Expo.FileSystem.cacheDirectory;
      default:
        return null;
    }
  }
}

export default new FileSystem();
