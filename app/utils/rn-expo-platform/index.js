import Firebase from 'firebase';
import Analytics from './Analytics';
import DeviceInfo from './DeviceInfo';
import FileSystem from './FileSystem';
import LocalKeyValueStorage from './LocalKeyValueStorage';

export default { Analytics, DeviceInfo, FileSystem, LocalKeyValueStorage, Firebase };
