import { Asset, FileSystem } from 'expo';
import { Util } from 'teko-js-sale-library';
import moment from 'moment-timezone';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../resources/fonts/selection.json';

export const IconSAT = createIconSetFromIcoMoon(icoMoonConfig, 'saletool');

export function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export function convertTime(time) {
  let day = convertDay(new Date(time).getDay());
  return `${day}, ${String(moment(time).format('DD/MM/YYYY'))}`;
}

export function formatDate(time) {
  return moment(new Date(time))
    .tz('Asia/Ho_Chi_Minh')
    .format('DD/MM/YYYY - HH:mm');
}

export function compareTime(time1, time2) {
  let t1 = new Date(time1).getTime();
  let t2 = new Date(time2).getTime();
  return t1 > t2 ? 1 : -1;
}

export function convertDay(day) {
  if (day === 0) return 'Chủ nhật';
  if (day === 1) return 'Thứ hai';
  if (day === 2) return 'Thứ ba';
  if (day === 3) return 'Thứ tư';
  if (day === 4) return 'Thứ năm';
  if (day === 5) return 'Thứ sáu';
  if (day === 6) return 'Thứ bảy';
}

export function getInventory(inventories) {
  let totalInventory = 0;
  for (let storeCode in inventories) {
    totalInventory += inventories[storeCode].quantity;
  }

  return totalInventory;
}

export function getPageInOffsaleUrl(url) {
  let page = Util.Text.getQueryValueFromUrl('page', url);
  return page ? parseInt(page) : -1;
}

export function getObjectByKeys(src, ...keys) {
  let obj = {};
  for (let key of keys) {
    obj[key] = src[key];
  }

  return obj;
}

export function isSubset(subset, superset, ignoreKeys = []) {
  return Object.keys(subset).every(key => ignoreKeys.includes(key) || subset[key] === superset[key]);
}

export function isEqualObjectByKeys(a, b, ...keys) {
  return keys.every(key => a[key] === b[key]);
}

export function standardizeInventoriesProducts(PROVINCES, shopList, stock) {
  let storeProduct = {};
  if (shopList) {
    for (let branch_id of Object.keys(stock)) {
      try {
        // let store_in_stock = stock[stockCode];
        // if (stock[branch_id].sale_quantity === 0) continue;
        let store = shopList[branch_id];
        let province_code = String(store.profile.district_id).substring(0, 2);

        let { address, location } = store.profile;
        let { region_id, label } = PROVINCES.find(item => item.key === province_code);
        if (!storeProduct[region_id]) storeProduct[region_id] = {};
        if (!storeProduct[region_id][label]) storeProduct[region_id][label] = {};
        storeProduct[region_id][label][branch_id] = {
          ...stock[branch_id],
          address,
          location,
          name: stock[branch_id].branch_name,
          quantity: stock[branch_id].sale_quantity,
        };
      } catch (error) {
        console.log('error', error, branch_id);
      }
    }
  }
  return storeProduct;
}

export function mapStatusToType(status) {
  if (status) {
    if (status === 'sale' || status === 'failure' || status === 'confirmed') return { key: 0, label: 'Chờ xử lý' };
    if (status === 'delivered') return { key: 3, label: 'Giao thành công' };
    if (status === 'exported') return { key: 1, label: 'Đã xuất hàng' };
    if (status === 'delivering') return { key: 2, label: 'Đang giao' };
    if (status === 'canceled') return { key: 4, label: 'Đã hủy' };
  }
  return { key: -1, label: 'Không xác định' };
}

export function mapMoreOptionsToParams(options) {
  if (!options) return null;
  let { brands, categories, priceFrom, priceTo } = options;
  let extra = { isExtra: true };
  let brand, category;
  if (brands)
    brand = brands
      .filter(item => item.isChecked)
      .map(item => item.key)
      .join();
  if (categories)
    category = categories
      .filter(item => item.isChecked)
      .map(item => item.key)
      .join();
  if (brand) extra.brand = brand;
  if (category) extra.category = category;
  if (priceFrom) extra.priceFrom = priceFrom;
  if (priceTo) extra.priceTo = priceTo;
  return extra;
}

export function properNumberString(str) {
  if (str === '-') return str;

  let properedString = str.trim().replace(/[^-\d.-]/g, '');
  properedString = properedString.replace(/^0+/g, '');
  properedString = properedString.replace(/(-{2})/g, '-');
  let number = parseInt(properedString) || 0;
  return properedString === '-' ? properedString : String(number);
}

export function onlyNumber(str) {
  return str.trim().replace(/[^\d]/g, '');
}

export function arraysEqual(arr1, arr2, compareFunction = (a, b) => a === b) {
  if (arr1 === arr2) return true;
  if (!arr1 || !arr2) return false;
  if (arr1.length !== arr2.length) return false;

  return (
    arr1.every(a1 => arr2.find(a2 => compareFunction(a1, a2))) && arr2.every(a2 => arr1.find(a1 => compareFunction(a1, a2)))
  );
}

export async function readJsonAssetFile(module) {
  try {
    let asset = Asset.fromModule(module);
    await asset.downloadAsync();
    let data = await FileSystem.readAsStringAsync(asset.localUri);
    data = JSON.parse(data);
    return data;
  } catch (error) {
    return null;
  }
}

export function getTotalExtraDiscount(order) {
  let totalDiscount = 0;
  if (order) {
    let { invoice, items } = order;
    totalDiscount += invoice.grand_discount;
    for (let item of items) {
      totalDiscount += (item && item.product && item.product.discount_item * item.quantity) || 0;
    }
  }
  return totalDiscount;
}

export function getTotalDiscount(order) {
  let totalDiscount = 0;
  if (order) {
    let { invoice } = order;
    totalDiscount = invoice.total - invoice.final;
  }
  return totalDiscount;
}
