import moment from 'moment';
import GeoLocation from './GeoLocation';
import * as Const from '../constants';

export default class OmOrder {
  constructor(cart, userProfile, storeProfile, waitForApproval) {
    this.cart = cart;
    this.userProfile = userProfile;
    this.storeProfile = storeProfile;
    this.waitForApproval = waitForApproval;
  }

  getOrderParams = () => {
    let ret = this.getCommonParams();
    ret = { ...ret, ...this.getCustomerParams() };
    ret = { ...ret, ...this.getBillingParams() };
    ret = { ...ret, ...this.getPostpaidParams() };
    ret = { ...ret, ...this.getShippingParams() };
    ret = { ...ret, ...this.getItemsParams() };
    return ret;
  };

  getCommonParams = () => {
    let { note, installation, technical_inspection } = this.cart.info;
    return {
      salesmanId: this.userProfile.teko_id,
      waitForApproval: this.waitForApproval,
      storeId: this.cart.branch,
      storeName: this.storeProfile.name,
      totalDiscount: 0,
      note,
      channel: 1,
      install: installation ? installation : false,
      technicalSupport: technical_inspection ? technical_inspection : false,
      orderDate: moment(this.cart.created_at).unix(),
      deposit: false
    };
  };

  getCustomerParams = () => {
    let { customer, billing, customer_detail } = this.cart.info;
    let targetId = customer_detail.customer_type_code === Const.CUSTOMER_TYPE.INDIVIDUAL ? customer.id : billing.id;
    let targetAsiaId =
      customer_detail.customer_type_code === Const.CUSTOMER_TYPE.INDIVIDUAL ? customer.asia_id : billing.asia_id;

    let ret = {
      customer: {
        id: targetId,
        name: customer.name,
        address: GeoLocation.makeFullAddress(customer),
        phone: customer.phone
      }
    };

    if (targetAsiaId) {
      ret.customer.asiaCrmId = targetAsiaId;
    }
    return ret;
  };

  getBillingParams = () => {
    let ret = {};
    let { customer, billing } = this.cart.info;

    if (billing.isTax) {
      ret.billingInfo = {
        name: billing.name || customer.name,
        address: billing.address || customer.address,
        telephone: customer.phone,
        taxCode: billing.tax_code,
        billingType: billing.isTaxExemption ? '02' : '01', // To-do
        printAfter: 0,
        printPretaxPrice: false
      };
    }

    return ret;
  };

  getShippingParams = () => {
    let ret = {};
    let { shipping } = this.cart.info;

    ret.shipDate = this.getShipDate();
    if (shipping.delivery && shipping.commune_id.length > 0) {
      ret.shippingInfo = {
        address: GeoLocation.makeFullAddress(shipping),
        name: shipping.receive_name ? shipping.receive_name : '',
        street: shipping.address ? shipping.address : '',
        addressCode: shipping.commune_id,
        email: '',
        telephone: shipping.receive_phone ? shipping.receive_phone : ''
      };
      ret.delivery = true;
    } else {
      ret.delivery = false;
    }

    return ret;
  };

  getShipDate = () => {
    let { shipping } = this.cart.info;
    let minShipDate = Date.now() + 3600 * 1000;
    let finalShipDate = shipping.time ? (shipping.time < minShipDate ? minShipDate : shipping.time) : minShipDate;
    return Math.round(finalShipDate / 1000);
  };

  getPostpaidParams = () => {
    let { postpaid_payment } = this.cart.info;
    let cartHavePostpaid = postpaid_payment && postpaid_payment.postpaid ? true : false;
    return {
      paymentDuration: cartHavePostpaid ? postpaid_payment.postpaid_period : 0,
      maxPostpaidAmount: cartHavePostpaid ? postpaid_payment.amount : 0
    };
  };

  getItemsParams = () => {
    return {
      items: this.cart.cart.orderlines.map(this.convertCartItemToOmItem)
    };
  };

  convertCartItemToOmItem = cartItem => {
    let { info } = this.cart;
    let { product, quantity, price, discount, warranty, vat } = cartItem;
    let unitPrice = price ? price : 0;
    let sku = String(product);

    return {
      sku,
      quantity,
      unitPrice,
      vat: info.billing.isTaxExemption ? 0 : vat,
      discountAmount: info.billing.isTaxExemption
        ? discount / quantity + (unitPrice - discount / quantity) * 0.1
        : discount / quantity,
      warranty: parseInt(warranty),
      discountReason: this.getDiscountReasonsPerSku(sku)
    };
  };

  getDiscountReasonsPerSku = sku => {
    let { cart } = this.cart;
    let discountReason = '';

    for (let p of cart.product_sale) {
      if (p.sku === sku && p.promotion && p.promotion.reason) discountReason = p.promotion.reason;
    }

    for (let p of cart.products) {
      // thêm lí do giảm giá cho phí dịch vụ
      if (sku === '1702079' || (sku === '1206838' && p.price === 0)) discountReason = 'GG000099';
    }

    for (let c of cart.combo_sale) {
      let skuInCombo = c.products.find(p => p.sku === sku);
      if (c.promotion && c.promotion.reason && skuInCombo) discountReason = c.promotion.reason;
    }

    if (discountReason === '' && cart.grand_discount && cart.grand_discount.reason) {
      // nếu xin giảm giá toàn đơn thì lấy lí do toàn đơn cho những sản phẩm không xin giảm giá / quà tặng
      discountReason = cart.grand_discount.reason;
    }

    return discountReason;
  };
}
