import firebase from 'firebase';

class ServerTime {
  constructor() {
    this.serverTime = Date.now();
    this.clientSyncTime = this.serverTime;
    this.appPauseTime = 0;
  }

  onAppPause = () => {
    this.appPauseTime = Date.now();
  };

  onAppResume = () => {
    if (Date.now() - this.appPauseTime > 600000) {
      // if time before app pause and after app resume greater than 10 minutes,
      // maybe user changed their phone time trying to hack so we need resync server time.
      // (if he really paused for 10 minutes, it's still ok to resync server time)
      this.sync();
    }
  };

  get = () => {
    return this.serverTime + Date.now() - this.clientSyncTime;
  };

  sync = async () => {
    this.serverTime = await this.getRealServerTimestamp();
    this.clientSyncTime = Date.now();
    // console.log('server-time: ', this.serverTime);
    // console.log('sync at: ', this.clientSyncTime);
  };

  getRealServerTimestamp = async () => {
    try {
      let offset = await firebase
        .database()
        .ref('.info/serverTimeOffset')
        .once('value');
      return Date.now() + offset.val();
    } catch (error) {
      return Date.now();
    }
  };
}

export default new ServerTime();
