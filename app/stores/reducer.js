import { combineReducers } from 'redux';
import appState from './appState/reducer';
import product from './product/reducer';
import cart from './cart/reducer';
import firebase from './firebase/reducer';
import screenState from './screenState/reducer';
import sale from './sale/reducer';
import customer from './customer/reducer';

// Combines all reducers to a single reducer function
const reducer = combineReducers({
  appState,
  product,
  cart,
  firebase,
  screenState,
  sale,
  customer,
});

export default reducer;
