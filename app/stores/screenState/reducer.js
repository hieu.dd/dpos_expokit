import * as actionTypes from './action-types';

const initialState = {
  ProductDetail: {
    addProduct: null,
    hidePromotion: false,
    isAddingProductToCombo: false,
  },
  ProductList: {
    shouldSearchInScreen: false,
    hideFilter: false,
    hideCategoryFilter: false,
    goToDetail: null,
  },
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.SET_SCREEN_STATE:
      return { ...state, [action.screen]: action.state };

    case actionTypes.UPDATE_SCREEN_STATE: {
      const currentScreenState = state[action.screen];
      if (currentScreenState) {
        return { ...state, [action.screen]: { ...currentScreenState, ...action.state } };
      } else {
        return state;
      }
    }

    case actionTypes.SET_ON_ADD_PRODUCT_IN_PRODUCT_DETAIL:
      return { ...state, ProductDetail: { ...state.ProductDetail, addProduct: action.addProduct } };

    default:
      return state;
  }
}
