import * as types from './action-types';

export function updateScreenState(screen, state) {
  return {
    type: types.UPDATE_SCREEN_STATE,
    screen,
    state,
  };
}

export function setScreenState(screen, state) {
  return {
    type: types.SET_SCREEN_STATE,
    screen,
    state,
  };
}

export function setAddProductInProductDetail(addProduct) {
  return {
    type: types.SET_ON_ADD_PRODUCT_IN_PRODUCT_DETAIL,
    addProduct,
  };
}
