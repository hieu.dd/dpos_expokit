import * as types from './action-types';
import * as consts from '../../config/consts';
import products_asia_hardcoded from '../../config/products_asia_hardcoded';

const initialState = {
  categories: {
    isLoading: false,
    error: '',
    offset: 0,
    data: [],
  },
  suggestions: [],
  price_others_company: {
    isLoading: false,
    error: '',
    data: [],
  },
  products: {
    [consts.all_category]: {
      isLoading: false,
      isLoadingMore: false,
      error: '',
      data: [],
      moreOptions: {},
    },
  },
  product_details: { ...products_asia_hardcoded },
  magento_details: {},
  import_prices: {},
  current_category: consts.all_category,
  search_params: {},
  product_statuses: {},
};

export default function reduce(state = initialState, action = {}) {
  let products = { ...state.products };
  let categories = { ...state.categories };
  let product_details = { ...state.product_details };
  let price_others_company = { ...state.price_others_company };
  let current_category = state.current_category;
  switch (action.type) {
    case types.CHANGE_CATEGORY:
      if (!products[action.category]) {
        products = {
          ...products,
          [action.category]: {
            isLoading: true,
            isLoadingMore: false,
            error: '',
            data: [],
          },
        };
      }
      return { ...state, current_category: action.category, products };
    case types.SAVE_QUERY:
      products[current_category].query = action.query;
      return { ...state, products };
    case types.FETCH_PRICE_OTHERS_COMPANY_START:
      price_others_company.isLoading = true;
      return { ...state, price_others_company };
    case types.FETCH_PRICE_OTHERS_COMPANY_RESULT:
      price_others_company.error = action.success ? '' : action.data;
      price_others_company.data = action.success ? action.data : [];
      price_others_company.isLoading = false;
      return { ...state, price_others_company };
    case types.FETCH_CATEGORIES_START:
      categories.isLoading = true;
      return { ...state, categories };
    case types.FETCH_CATEGORIES_RESULT:
      categories.isLoading = false;
      categories.error = action.success ? '' : action.data;
      categories.data = action.success ? action.data : [];
      return { ...state, categories };
    case types.FETCH_PRODUCTS_START:
      products = {
        ...products,
        [current_category]: { ...products[current_category], isLoading: true, isLoadingMore: false, data: [] },
      };
      return { ...state, products };

    case types.FETCH_PRODUCTS_RESULT:
      products = {
        ...products,
        [current_category]: {
          ...products[current_category],
          isLoading: false,
          next_page: action.next_page,
          error: action.success ? '' : action.data,
          data: action.success ? action.data : [],
          moreOptions: action.moreOptions ? action.moreOptions : products[current_category].moreOptions || {},
        },
      };
      return { ...state, products, suggestions: action.suggestions };

    case types.SAVE_MORE_OPTIONS:
      products = {
        ...products,
        [current_category]: {
          ...products[current_category],
          moreOptions: action.moreOptions,
        },
      };
      return { ...state, products };
    case types.FETCH_MORE_PRODUCTS_START:
      products = { ...products, [current_category]: { ...products[current_category], isLoadingMore: true } };
      return { ...state, products };
    case types.FETCH_MORE_PRODUCTS_RESULT:
      products = {
        ...products,
        [current_category]: {
          ...products[current_category],
          isLoadingMore: false,
          next_page: action.next_page,
          error: action.success ? '' : action.data,
          data: action.success ? [...products[current_category].data, ...action.data] : products[current_category].data,
        },
      };
      return { ...state, products };
    case types.FETCH_PRODUCT_DETAIL_START:
      if (!product_details[action.sku]) {
        product_details[action.sku] = {
          isLoading: true,
          error: '',
        };
      } else {
        product_details[action.sku] = { ...product_details[action.sku], isLoading: true };
      }
      return { ...state, product_details };
    case types.FETCH_PRODUCT_DETAIL_RESULT:
      return fetchProductDetail(state, action);

    case types.FETCH_MAGENTO_PRODUCT_DETAIL:
      return fetchMagentoProductDetail(state, action);

    case types.FETCH_MAGENTO_PRODUCT_LIST_DETAIL:
      return fetchMagentoProductListDetail(state, action);

    case types.FETCH_IMPORT_PRICE_START:
      return {
        ...state,
        import_prices: {
          ...state.import_prices,
          [action.sku]: {
            isLoading: true,
            timestamp: -1,
          },
        },
      };

    case types.FETCH_IMPORT_PRICE_RESULT:
      return {
        ...state,
        import_prices: {
          ...state.import_prices,
          [action.sku]: {
            timestamp: action.timestamp,
            price: action.price,
            isLoading: false,
          },
        },
      };
    case types.SAVE_PRODUCT_SEARCH_PARAMS:
      return { ...state, search_params: action.params };

    case types.FETCH_PRODUCT_STATUS_RESULT: {
      let product_statuses = { ...state.product_statuses };
      product_statuses[action.sku] = action.data;
      return { ...state, product_statuses };
    }

    case types.FETCH_PRODUCT_STATUS_LIST_RESULT:
      return fetchProductStatusListResult(state, action);

    default:
      return state;
  }
}

function fetchProductDetail(state, action) {
  let { product_details } = state;

  let oldProduct = product_details[action.sku];
  let oldProductDetail = (oldProduct && oldProduct.detail) || {};
  let product = {
    isLoading: false,
    error: action.success ? null : action.data,
    timeUpdate: new Date().getTime(),
    hasMagentoDetail: oldProduct.hasMagentoDetail,
    detail: action.success ? { ...oldProductDetail, ...action.data } : null,
  };

  return {
    ...state,
    product_details: {
      ...product_details,
      [action.sku]: product,
    },
  };
}

function fetchMagentoProductListDetail(state, action) {
  const { productList } = action;
  for (let product of productList) {
    state = addProductMagentoDetail(state, { product });
  }

  return state;
}

function addProductMagentoDetail(state, { product }) {
  let { magento_details } = state;

  if (!magento_details[product.sku]) {
    magento_details = {
      ...magento_details,
      [product.sku]: product,
    };
  }

  return { ...state, magento_details };
}

function fetchMagentoProductDetail(state, action) {
  let { product_details } = state;
  let product = product_details[action.sku];

  if (product && action.success) {
    product = {
      ...product,
      hasMagentoDetail: true,
      detail: {
        ...product.detail,
        image: action.data.image,
        attributes: action.data.attributes,
        // description: action.data.description
      },
    };
  } else {
    product = {
      isLoading: false,
      error: action.success ? '' : action.data,
      detail: action.data,
      timeUpdate: 0,
    };
  }

  product_details[action.sku] = product;

  return {
    ...state,
    product_details: {
      ...product_details,
      [action.sku]: product,
    },
  };
}

function fetchProductStatusListResult(state, action) {
  let { product_statuses } = state;
  const { productStatusList } = action;

  for (let productStatus of productStatusList) {
    product_statuses = {
      ...product_statuses,
      [productStatus.sku]: productStatus,
    };
  }

  return {
    ...state,
    product_statuses,
  };
}
