import * as customerActionTypes from './action-types';

export const initialCustomerInfo = {
  billing: {},
  hasShpping: false,
  shipping: {},
  hasDeposit: false,
  deposit: { cash: null, method: null },
};

export const initialState = {
  searchResults: [{ id: 39146, contacts: [] }],
  customerInfo: initialCustomerInfo,
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case customerActionTypes.SET_SEARCH_RESULTS:
      return { ...state, searchResults: action.searchResults || [] };
    case customerActionTypes.ADD_CONTACT: {
      let { customer_id, contact } = action;
      let searchResults = [...state.searchResults];
      let index = searchResults.findIndex(customer => customer.id === customer_id);
      let customer = index === -1 ? { id: 39146, contacts: [] } : searchResults[index];
      customer.contacts.push(contact);
      if (index !== -1) {
        searchResults.splice(index, 1, customer);
      } else {
        searchResults.push(customer);
      }
      return { ...state, searchResults };
    }
    default:
      return state;
  }
}
