import SaleService from 'teko-js-sale-library/packages/Service/SaleService';
import Tracking from 'teko-js-sale-library/packages/Tracking';

import TRACK from '../../config/TrackingPrototype';
import * as strings from '../../resources/strings';
import * as utils from '../../utils';
import { verifyVoucherCode } from '../../utils/voucher';
import * as cartUtils from '../../utils/cart';
import * as saleUtils from '../../utils/sale';
import * as firebaseUtils from '../../utils/firebase';
import * as types from './action-types';
import * as cartActions from '../cart/actions';
import * as appStateActions from '../appState/actions';
import * as customerActions from '../customer/actions';
import * as customerUtils from '../../utils/customer';

import CartService from '../../services/CartService';
import RootNavigationService from '../../services/navigation/RootNavigationService';
import moment from 'moment';
import * as firebaseActions from '../../stores/firebase/actions';

export function createOrder({ payment, onSuccess, onFailure }) {
  return async function(dispatch, getState) {
    let orderlines, invoice, customerInfo;
    let trackingResult = TRACK.CONSTANT.SUCCESS;
    let createMagentoOrderFailed = false;

    try {
      dispatch(appStateActions.showLoadingModal({ loadingText: strings.creating_order }));

      let { cart, product } = getState();
      let params = {
        salesmanId: 12,
        waitForApproval: false,
        storeId: 'CP09',
        storeName: 'Showroom Phong Vũ Hà Nội',
        exportStoreId: 'CP09',
        channel: 1,
        install: false,
        technicalSupport: false,
        deposit: false,
        paymentDuration: 0,
        maxPostpaidAmount: 0,
        shipDate: moment(new Date().getTime() + 86400000).unix(),
        orderDate: moment().unix(),
        totalDiscount: 0,
        delivery: false,
      };
      customerInfo = cart.customerInfo; // customerInfo must be getted from cart, not currentCart
      let crmCustomer = customerUtils.mapCRMparamsFromCustomer(
        getState().customer.searchResults.find(item => item.id === customerInfo.id)
      );

      if (crmCustomer) {
        dispatch(customerActions.saveCustomerToCRM(crmCustomer));
      }
      params.customer = saleUtils.getCustomerFromCustomerInfo(customerInfo);
      if (customerInfo.shipping) {
        params.shippingInfo = saleUtils.getShippingInfoFromCustomerInfo(customerInfo);
        params.delivery = true;
      }

      let currentCart = cartUtils.getCurrentCart(cart);
      let productDetails = product.product_details;
      orderlines = cartUtils.makeOrderlinesFromCart(currentCart, productDetails);
      let firebaseUpdates = firebaseUtils.getFirebaseUpdates(currentCart);
      let lockQuantityRet = await firebaseUtils.lockQuantityOnFirebase(firebaseUpdates);

      if (!lockQuantityRet.ok) {
        trackingResult = TRACK.CONSTANT.API_REQUEST_FAIL;
        throw new Error(lockQuantityRet.data);
      }

      // orderlines = saleUtils.addServiceFee(orderlines, currentCart, customerInfo, productDetails);
      let items = saleUtils.makeOrderLinesBEToOrderLinesOm(orderlines);
      params.items = items;
      invoice = saleUtils.getInvoiceFromOrderlines(orderlines);
      let ret = await CartService.createOrderOM(params);
      if (!ret.data || ret.data.error) {
        trackingResult = TRACK.CONSTANT.API_REQUEST_FAIL;
        throw new Error(JSON.stringify(ret.data));
      } else if (ret.status === 'failure') {
        trackingResult = TRACK.CONSTANT.API_REQUEST_FAIL;
        createMagentoOrderFailed = true;
        throw new Error(saleUtils.getMagentoMessage(ret.data.magento_msg));
      }
      Tracking.trackEvent(TRACK.EVENT.CREATE_ORDER, trackingResult, ret.data.id, customerInfo, orderlines, invoice);
      dispatch({
        type: types.SAVE_CURRENT_TRANSACTION,
        data: { ...ret.data.result, invoice },
      });
      if (payment === 'VNPAYQR') {
        await dispatch(onQRPayment(ret.data.result.id, invoice.price));
      }

      await firebaseUtils.updatePromotionQuantity(firebaseUpdates, ret.data);

      dispatch(cartActions.clearCart());
      dispatch(cartActions.saveTempCustomerInfo(null));
      dispatch(appStateActions.hideLoadingModal());
      onSuccess && onSuccess();
    } catch (error) {
      trackingResult = trackingResult !== TRACK.CONSTANT.API_REQUEST_FAIL ? TRACK.CONSTANT.CODE_ERROR : null;
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'createOrder', error.message, error.stack);
      Tracking.trackEvent(TRACK.EVENT.CREATE_ORDER, trackingResult, null, customerInfo, orderlines, invoice);
      let errorTitle = createMagentoOrderFailed ? strings.magento_error : strings.create_order_error;
      onFailure();
      dispatch(appStateActions.hideLoadingModal());
    }
  };
}

export function onQRPayment(id, amount) {
  return async function(dispatch, getState) {
    dispatch(appStateActions.showLoadingModal({ loadingText: strings.creating_order }));

    dispatch(firebaseActions.listenFirebasePayment(id));
    let payParams = {
      id,
      amount,
    };
    let ret = await CartService.creatPayNVPAYQR(payParams);
    if (ret.ok) {
      dispatch({
        type: types.SAVE_CURRENT_TRANSACTION,
        data: ret.data.result,
      });
    }
    dispatch(appStateActions.hideLoadingModal());
  };
}

export function clearPayment() {
  return {
    type: types.CLEAR_PAYMENT,
  };
}

export function onCancelQRPay() {
  return {
    type: types.CANCEL_QRPAY,
  };
}

export function creatOrderOM({ payment, onSuccess, onFailure }) {
  return async function(dispatch, getState) {
    try {
      let customer = { customer_name: 'Đỗ Hiếu', customer_phone: '0392222170' };

      let currentCart = cartUtils.getCurrentCart(getState().cart);
      let waitForApproval = cartUtils.checkAdditionalInCart(currentCart);

      if (waitForApproval) {
        await createQuotation({ customer, onSuccess, onFailure })(dispatch, getState);
      } else {
        await createOrder({ payment, onSuccess, onFailure })(dispatch, getState);
      }
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'creatOrderOM', error.message, error.stack);
    }
  };
}

export function sendQuotationToBE({ onSuccess, onFailure }) {
  return async function(dispatch, getState) {
    try {
      // let customerId = await customerActions.getCustomerOrCreate({ onSuccess, onFailure })(dispatch, getState);
      let customer = { customer_name: 'Đỗ Hiếu', customer_phone: '0392222170' };
      // if (!customerId) {
      //   return;
      // }

      await createQuotation({ customer, onSuccess, onFailure })(dispatch, getState);
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'sendQuotationToBE', error.message, error.stack);
    }
  };
}

export function createQuotation({ customer, onSuccess, onFailure }) {
  return async function(dispatch, getState) {
    let orderlines, invoice, customerInfo;
    let waitForApproval = false;

    try {
      let { cart, product } = getState();
      customerInfo = cart.customerInfo; // customerInfo must be getted from cart, not currentCart
      let currentCart = cartUtils.getCurrentCart(cart);
      let productDetails = product.product_details;
      orderlines = cartUtils.makeOrderlinesFromCart(currentCart, productDetails);
      let additionalOrderInfo = cartUtils.getAdditionalOrderInfo(customerInfo);
      let discountOrder = cartUtils.getAdditionalDiscount(currentCart);
      let voucherData = saleUtils.getVoucherDataInCart(currentCart);
      let shipping = saleUtils.getShippingFromCustomerInfo(customerInfo);

      orderlines = saleUtils.addServiceFee(orderlines, currentCart, customerInfo, productDetails);
      invoice = saleUtils.getInvoiceFromOrderlines(orderlines);

      waitForApproval = cartUtils.checkAdditionalInCart(currentCart);
      let discountDesc = waitForApproval ? 'Xin giảm giá' : '';
      let actionMessage = waitForApproval ? strings.creating_approval_order : strings.creating_quotation;

      dispatch(appStateActions.showLoadingModal({ loadingText: actionMessage }));
      let extraInfo = currentCart;

      let ret = await SaleService.createQuotation({
        customer,
        orderlines,
        shipping,
        billing: customerInfo.billing,
        note: customerInfo.note,
        waitForApproval,
        discountOrder,
        discountDesc,
        voucherData,
        additionalOrderInfo,
        extraInfo,
      });

      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      } else if (ret.data.status === 'failure') {
        throw new Error(saleUtils.getMagentoMessage(ret.data.magento_msg));
      }

      Tracking.trackEvent(
        TRACK.EVENT.CREATE_QUOTATION,
        TRACK.CONSTANT.SUCCESS,
        ret.data.id,
        customerInfo,
        orderlines,
        invoice,
        waitForApproval
      );

      dispatch(cartActions.clearCart());
      //clear temp customer info
      dispatch(cartActions.saveTempCustomerInfo(null));

      dispatch(appStateActions.hideLoadingModal());
      dispatch(
        appStateActions.showAnimationModal({
          animationType: 'success',
          title: waitForApproval ? strings.creating_approval_order_success : strings.create_quotation_success,
          actions: [
            {
              text: 'OK',
              onPress: () => {
                RootNavigationService.navigate('Main');
                RootNavigationService.navigate('Order');
              },
            },
          ],
        })
      );
    } catch (error) {
      dispatch(appStateActions.hideLoadingModal());
      dispatch(
        appStateActions.showAnimationModal({
          animationType: 'error',
          title: strings.create_order_failure,
          detail: error.message,
        })
      );

      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'createQuotation', error.message, error.stack);
      Tracking.trackEvent(
        TRACK.EVENT.CREATE_QUOTATION,
        TRACK.CONSTANT.API_REQUEST_FAIL,
        null,
        customerInfo,
        orderlines,
        invoice
      );
    }
  };
}

export function loadOrderDetail(orderId, query, onResult) {
  return async function(dispatch, getState) {
    try {
      let ret = await SaleService.getOrderDetail(orderId, query);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let rawOrder = ret.data;
      let order = saleUtils.mapOrderToLocal(rawOrder);
      if (saleUtils.isQuotation(rawOrder)) {
        dispatch(saveQuotationToLocal(order));
      }
      dispatch(saveOrderToLocal(order));
      onResult && onResult(true);
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadOrderDetail', error.message, error.stack);
      onResult && onResult(false);
    }
  };
}

export function saveOrderToLocal(order) {
  return {
    type: types.SAVE_ORDER_TO_LOCAL,
    order,
  };
}

export function saveQuotationToLocal(quotation) {
  return {
    type: types.SAVE_QUOTATION_TO_LOCAL,
    quotation,
  };
}

export function loadOrders(query = {}) {
  return async function(dispatch, getState) {
    try {
      dispatch(markLoadingOrders());
      let ret = await SaleService.getOrders(query);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let orders = saleUtils.convertOrdersToLocal(ret.data.results);
      let nextPage = utils.getPageInOffsaleUrl(ret.data.next);
      dispatch(setLoadOrdersResult({ orders, nextPage }));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadOrders', error.message, error.stack);

      dispatch(setLoadOrdersResult({ orders: [], nextPage: -1 }));
    }
  };
}

export function loadTransactions(timeFilter) {
  return async function(dispatch, getState) {
    try {
      dispatch({
        type: types.SAVE_TIME_FILTER_TRANSACTION,
        timeFilter,
      });
      dispatch({ type: types.LOADING_TRANSACTION });
      let query = {
        page_size: 10,
      };
      let { dateFrom, dateTo } = timeFilter;
      if (dateFrom && dateFrom !== '') query.created_from = String(moment(dateFrom).format('YYYY-MM-DD'));
      if (dateTo && dateTo !== '') query.created_to = String(moment(dateTo).format('YYYY-MM-DD'));

      let ret = await SaleService.getTransactions(query);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let transaction = ret.data;
      dispatch(loadTransactionResult(transaction));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadTransactions', error.message, error.stack);

      dispatch(loadTransactionResult(null));
    }
  };
}

export function markLoadingOrders() {
  return {
    type: types.MARK_LOADING_ORDERS,
  };
}

export function setLoadOrdersResult({ orders, nextPage }) {
  return {
    type: types.LOAD_ORDERS_RESULT,
    orders,
    nextPage,
  };
}

export function loadTransactionResult(transaction) {
  return { type: types.LOAD_TRANSACTION_RESULT, transaction };
}

export function loadMoreOrders(query) {
  return async function(dispatch, getState) {
    try {
      let saleState = getState().sale;
      let page = saleUtils.getOrderPageFromState(saleState);
      if (page < 0 || saleState.loadingMoreOrders) return;

      dispatch(markLoadingMoreOrders());
      let ret = await SaleService.getOrders(query, page);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let orders = saleUtils.convertOrdersToLocal(ret.data.results);
      let nextPage = utils.getPageInOffsaleUrl(ret.data.next);
      dispatch(setLoadMoreOrdersResult({ orders, nextPage }));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadMoreOrders', error.message, error.stack);

      dispatch(setLoadMoreOrdersResult({ orders: [], nextPage: -1 }));
    }
  };
}

export function loadMoreTransactions() {
  return async function(dispatch, getState) {
    try {
      let { transaction, timeFilter, isLoadingTransaction } = getState().sale;
      let page = transaction.transactions.next;
      if (!page || page < 0 || isLoadingTransaction) return;
      let query = {
        page,
        page_size: 10,
      };
      let { dateFrom, dateTo } = timeFilter;
      if (dateFrom && dateFrom !== '') query.created_from = String(moment(dateFrom).format('YYYY-MM-DD'));
      if (dateTo && dateTo !== '') query.created_to = String(moment(dateTo).format('YYYY-MM-DD'));
      // dispatch({ type: types.LOADING_TRANSACTION });
      let ret = await SaleService.getTransactions(query);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let data = ret.data;
      dispatch(loadMoreTransactionsResult(data));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadMoreTransactions', error.message, error.stack);

      dispatch(loadMoreTransactionsResult(null));
    }
  };
}

export function markLoadingMoreOrders() {
  return {
    type: types.MARK_LOADING_MORE_ORDERS,
  };
}

export function setLoadMoreOrdersResult({ orders, nextPage }) {
  return {
    type: types.LOAD_MORE_ORDERS_RESULT,
    orders,
    nextPage,
  };
}

export function loadMoreTransactionsResult(data) {
  return {
    type: types.LOAD_MORE_TRANSACTION_RESULT,
    transaction: data,
  };
}

export function loadQuotations(query) {
  return async function(dispatch, getState) {
    try {
      dispatch(markLoadingQuotations());
      let ret = await SaleService.getQuotations(query);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let quotations = saleUtils.convertOrdersToLocal(ret.data.results);
      let nextPage = utils.getPageInOffsaleUrl(ret.data.next);
      dispatch(setLoadQuotationsResult({ quotations, nextPage }));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadQuotations', error.message, error.stack);

      dispatch(setLoadQuotationsResult({ quotations: [], nextPage: -1 }));
    }
  };
}

export function markLoadingQuotations() {
  return {
    type: types.MARK_LOADING_QUOTATIONS,
  };
}

export function setLoadQuotationsResult({ quotations, nextPage }) {
  return {
    type: types.LOAD_QUOTATIONS_RESULT,
    quotations,
    nextPage,
  };
}

export function loadMoreQuotations(query) {
  return async function(dispatch, getState) {
    try {
      let saleState = getState().sale;
      let page = saleUtils.getQuotationPageFromState(saleState);
      if (page < 0 || saleState.loadingMoreQuotations) return;

      dispatch(markLoadingMoreQuotations());
      let ret = await SaleService.getQuotations(query, page);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let quotations = saleUtils.convertQuotationsToLocal(ret.data.results);
      let nextPage = utils.getPageInOffsaleUrl(ret.data.next);
      dispatch(setLoadMoreQuotationsResult({ quotations, nextPage }));
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'loadMoreQuotations', error.message, error.stack);

      dispatch(setLoadMoreQuotationsResult({ quotations: [], nextPage: -1 }));
    }
  };
}

export function withDrawRequest(amount, note = '', callback) {
  return async function(dispatch, getState) {
    try {
      let ret = await SaleService.withdrawRequest(amount, note);
      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      dispatch(
        appStateActions.showAnimationModal({
          message: 'Hoa hồng sẽ được gửi vào tài khoản ngân hàng của bạn sau 3-5 ngày',
          animationType: 'success',
          title: 'Chúng tôi đã nhận được yêu cầu rút tiền của bạn',
        })
      );
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'withDrawRequest', error.message, error.stack);
      dispatch(
        appStateActions.showAnimationModal({
          message: 'Hoa hồng sẽ được gửi vào tài khoản ngân hàng của bạn sau 3-5 ngày',
          title: 'Chúng tôi đã nhận được yêu cầu rút tiền của bạn',
          animationType: 'success',
        })
      );
    }
  };
}

export function markLoadingMoreQuotations() {
  return {
    type: types.MARK_LOADING_MORE_QUOTATIONS,
  };
}

export function setLoadMoreQuotationsResult({ quotations, nextPage }) {
  return {
    type: types.LOAD_MORE_QUOTATIONS_RESULT,
    quotations,
    nextPage,
  };
}

export function approveOrder(orderId, onResult) {
  return async dispatch => {
    let responseMessage = null;

    try {
      dispatch(appStateActions.showLoadingModal({ loadingText: strings.approving_order }));

      let ret = await SaleService.approveQuotation(orderId);
      if (!ret.ok) {
        if (ret.data && ret.data.message) {
          responseMessage = ret.data.message;
        }
        throw new Error(JSON.stringify(ret.data));
      }

      dispatch(
        appStateActions.showLoadingModal({
          message: strings.approve_order_success,
          actions: [
            {
              text: 'OK',
              onPress: () => {
                onResult && onResult(true);
              },
            },
          ],
        })
      );
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'approveOrder', error.message, error.stack);

      dispatch(
        appStateActions.showLoadingModal({
          message: responseMessage || strings.approve_order_failure,
          actions: [
            {
              text: 'OK',
              onPress: () => {
                onResult && onResult(false);
              },
            },
          ],
        })
      );
    }
  };
}

export function rejectOrder(orderId, reason, onResult) {
  return async dispatch => {
    let responseMessage = null;

    try {
      dispatch(appStateActions.showLoadingModal({ loadingText: strings.rejecting_order }));

      let ret = await SaleService.rejectQuotation(orderId, { manager_note: reason });
      if (!ret.ok) {
        if (ret.data && ret.data.message) {
          responseMessage = ret.data.message;
        }
        throw new Error(JSON.stringify(ret.data));
      }

      dispatch(
        appStateActions.showLoadingModal({
          message: strings.reject_order_success,
          actions: [
            {
              text: 'OK',
              onPress: () => {
                onResult && onResult(true);
              },
            },
          ],
        })
      );
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'rejectOrder', error.message, error.stack);

      dispatch(
        appStateActions.showLoadingModal({
          message: responseMessage || strings.approve_order_failure,
          actions: [
            {
              text: 'OK',
              onPress: () => {
                onResult && onResult(false);
              },
            },
          ],
        })
      );
    }
  };
}

export function updateQuotationToOrder(quotation, onResult) {
  return async (dispatch, getState) => {
    let createMagentoOrderFailed = false;
    let shouldShowMessage = false;

    try {
      dispatch(appStateActions.showLoadingModal({ loadingText: strings.creating_order }));

      // check voucher code in quotation
      let voucherCode = quotation.invoice.voucher_code;
      if (voucherCode) {
        const { user } = getState();

        let verifiedRet = await verifyVoucherCode(voucherCode, quotation.invoice.total, user);
        if (!verifiedRet.ok) {
          throw new Error(verifiedRet.message);
        }
      }

      // extra_info không có khi báo giá được tạo từ app cũ
      // ngược lại, báo giá được tạo từ app mới, cần phải kiểm tra số lương km trên firebase
      if (quotation.extra_info) {
        let cart = JSON.parse(quotation.extra_info);
        let firebaseUpdates = firebaseUtils.getFirebaseUpdates(cart);
        let lockQuantityRet = await firebaseUtils.lockQuantityOnFirebase(firebaseUpdates);
        if (!lockQuantityRet.ok) {
          shouldShowMessage = true;
          throw new Error(lockQuantityRet.data);
        }
      }

      let ret = await SaleService.updateQuotationToOrder(quotation.id);

      if (!ret.ok) {
        throw new Error(JSON.stringify(ret.data));
      }
      let orderRet = await SaleService.getOrderDetail(quotation.id);
      if (orderRet.data.status === 'failure') {
        createMagentoOrderFailed = true;
        throw new Error(saleUtils.getMagentoMessage(orderRet.data.magento_msg));
      }

      dispatch(appStateActions.hideLoadingModal());
      dispatch(
        appStateActions.showAnimationModal({
          animationType: 'success',
          title: strings.create_order_success,
          actions: [
            {
              text: 'Đóng',
              onPress: () => {
                onResult && onResult(true);
              },
            },
          ],
        })
      );
    } catch (error) {
      Tracking.trackEvent(TRACK.EVENT.APP_EXCEPTION, 'updateQuotationToOrder', error.message, error.stack);
      let errorTitle = createMagentoOrderFailed ? strings.magento_error : strings.create_order_error;

      dispatch(appStateActions.hideLoadingModal());
      dispatch(
        appStateActions.showAnimationModal({
          animationType: 'error',
          title: !shouldShowMessage ? errorTitle : '',
          detail: !shouldShowMessage ? error.message : '',
          message: shouldShowMessage ? error.message : '',
          actions: [
            {
              text: 'Đóng',
              onPress: () => {
                onResult && onResult(false);
              },
            },
          ],
        })
      );
    }
  };
}

export function removeQuotation(id) {
  return {
    type: types.REMOVE_QUOTATION,
    id,
  };
}
