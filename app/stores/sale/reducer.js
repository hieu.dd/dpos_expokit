import * as types from './action-types';

const initialState = {
  orders: [],
  loadingOrders: false,
  loadingMoreOrders: false,
  nextOrderPage: 1,
  quotations: [],
  loadingQuotations: false,
  loadingMoreQuotations: false,
  nextQuotationPage: 1,
  timeFilter: null,
  transaction: null,
  isLoadingTransaction: false,
  currentTransaction: {},
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.CANCEL_QRPAY: {
      return { ...state, currentTransaction: { ...state.currentTransaction, transactionId: null, paymentUrl: null } };
    }
    case types.CLEAR_PAYMENT:
      return { ...state, currentTransaction: {} };
    case types.SAVE_CURRENT_TRANSACTION:
      return { ...state, currentTransaction: { ...state.currentTransaction, ...action.data } };
    case types.SAVE_ORDER_TO_LOCAL:
      return saveOrderToLocal(state, action);

    case types.SAVE_QUOTATION_TO_LOCAL:
      return saveQuotationToLocal(state, action);

    case types.MARK_LOADING_ORDERS:
      return { ...state, loadingOrders: true };

    case types.LOAD_ORDERS_RESULT:
      return {
        ...state,
        orders: action.orders,
        nextOrderPage: action.nextPage,
        loadingOrders: false,
      };
    case types.LOADING_TRANSACTION:
      return {
        ...state,
        isLoadingTransaction: true,
      };
    case types.MARK_LOADING_MORE_ORDERS:
      return { ...state, loadingMoreOrders: true };
    case types.LOAD_MORE_ORDERS_RESULT:
      return {
        ...state,
        orders: [...state.orders, ...action.orders],
        nextOrderPage: action.nextPage,
        loadingMoreOrders: false,
      };
    case types.MARK_LOADING_QUOTATIONS:
      return { ...state, loadingQuotations: true };

    case types.LOAD_QUOTATIONS_RESULT:
      return {
        ...state,
        quotations: action.quotations,
        nextQuotationPage: action.nextPage,
        loadingQuotations: false,
      };
    case types.MARK_LOADING_MORE_QUOTATIONS:
      return { ...state, loadingMoreQuotations: true };
    case types.LOAD_MORE_QUOTATIONS_RESULT:
      return {
        ...state,
        quotations: [...state.quotations, ...action.quotations],
        nextQuotationPage: action.nextPage,
        loadingMoreQuotations: false,
      };
    case types.SAVE_TIME_FILTER_TRANSACTION:
      return { ...state, timeFilter: action.timeFilter };
    case types.LOAD_TRANSACTION_RESULT:
      return { ...state, transaction: action.transaction, isLoadingTransaction: false };
    case types.LOAD_MORE_TRANSACTION_RESULT:
      if (action.transaction) {
        let { data, next } = action.transaction.transactions;
        let transaction = { ...state.transaction };
        transaction.transactions.next = next;
        transaction.transactions.data = transaction.transactions.data.concat(data);
        return { ...state, transaction, isLoadingTransaction: false };
      }
      return { ...state, isLoadingTransaction: false };
    case types.REMOVE_QUOTATION:
      return removeQuotation(state, action);

    default:
      return state;
  }
}

function removeQuotation(state, action) {
  const { quotations } = state;
  const { id } = action;
  let newQuotations = quotations.filter(item => item.id !== id);

  return quotations.length !== newQuotations.length ? { ...state, quotations: newQuotations } : state;
}

function saveOrderToLocal(state, action) {
  let { order } = action;
  let { orders } = state;

  let existedOrder = orders.find(item => item.id == order.id);
  if (existedOrder) {
    orders = orders.map(item => {
      if (item.id === order.id) {
        return order;
      } else {
        return item;
      }
    });
  } else {
    orders = [...orders, order];
  }

  return { ...state, orders };
}

function saveQuotationToLocal(state, action) {
  let { quotation } = action;
  let { quotations } = state;

  let existedOrder = quotations.find(item => item.id == quotation.id);
  if (existedOrder) {
    quotations = quotations.map(item => {
      if (item.id === quotation.id) {
        return quotation;
      } else {
        return item;
      }
    });
  } else {
    quotations = [...quotations, quotation];
  }

  return { ...state, quotations };
}
