import * as utils from './utils';

export function checkVoucher(voucher, invoice, userScope, ignoreInvoice = false) {
  if (!voucher) return false;

  let { applyFrom, applyTo, startAt, endAt, quantity, unlimited, hideInApp } = voucher;
  let currentTime = Date.now();

  return (
    !hideInApp &&
    (invoice >= parseInt(applyFrom) || ignoreInvoice) &&
    (!applyTo || invoice < parseInt(applyTo)) &&
    utils.checkUserScope(voucher, userScope) &&
    currentTime >= startAt &&
    currentTime <= endAt &&
    (quantity > 0 || unlimited)
  );
}

export function isExpiredVoucher(voucher) {
  if (!voucher) return false;

  const { endAt } = voucher;
  const currentTime = Date.now();

  return currentTime > endAt;
}

export function hasToCheckVoucherQuantity(voucher) {
  let quantity = parseInt(voucher.quantity);
  return !isNaN(quantity) && !voucher.unlimited;
}

export function calculateVoucherDiscount(voucher, invoice) {
  if (!voucher || !invoice) return 0;

  let voucherDiscount = 0;
  if (typeof voucher.discount === 'object') {
    let discountObj = voucher.discount;
    if (discountObj.percent) {
      voucherDiscount = getVoucherDiscountByPercent(discountObj, invoice);
    } else if (discountObj.value) {
      voucherDiscount = getVoucherDiscountByValue(discountObj, invoice);
    }
  } else {
    voucherDiscount = voucher.discount || 0;
  }

  return parseInt(voucherDiscount);
}

export function getVoucherDiscountByPercent(discountObj, invoice) {
  let { percent } = discountObj;
  let voucherDiscount = getValueByPercent(invoice, percent);
  let maxDiscountValue = discountObj.max_value || invoice;

  return Math.min(voucherDiscount, maxDiscountValue);
}

export function getVoucherDiscountByValue(discountObj, invoice) {
  let { value } = discountObj;
  let voucherDiscount = discountObj.value || 0;
  let maxDiscountValue = voucherDiscount;
  if (discountObj.max_percent) {
    maxDiscountValue = getValueByPercent(invoice, discountObj.max_percent);
  }

  return Math.min(voucherDiscount, maxDiscountValue);
}

function getValueByPercent(value, percent) {
  return Math.floor((value * percent) / 100);
}
