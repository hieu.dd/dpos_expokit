export function getPromotionsNewBenefitPath(programKey, promoKey) {
  return `promotions-dpos/promotions_new/${programKey}/data/${promoKey}/benefit`;
}

export function getPromotionsNewQuantityLeftPath(programKey, promoKey) {
  return `promotions-dpos/promotions_new/${programKey}/data/${promoKey}/quantity_left`;
}

export function getPromotionsOldQuantityPath(programKey, promoKey) {
  return `promotions-dpos/promotions/${programKey}/data/${promoKey}/quantity`;
}

export function getVouchersQuantityPath(key) {
  return `promotions-dpos/promotions_other/voucher/${key}/quantity`;
}

export function getAppliedCodeVoucherPath(voucher) {
  return `promotions-dpos/applied_data/${voucher.key}/${voucher.code}`;
}
