import uuidv4 from 'uuid/v4';
import moment from 'moment-timezone';
import PromoManager from 'teko-promotion-parser';

import * as path from './path';
import { equalByKeys, getObjectProperty } from './utils';
import { equalBenefit, mapBenefitFromAsiaGifts, flatBenefitWithQuantity, getAllOfBenefit } from './benefit';
import { getPromotionsOld, isValidPromotion as isValidPromotionOld } from './promotionsOld';
import { getBenefitFromPromoNew, isActiveProgramForAgent } from './promotionsNew';

export function getNow() {
  let timestamp = Date.now();
  let current = moment(timestamp).tz('Asia/Ho_Chi_Minh');

  return {
    timestamp,
    date: current.format('YYYY-MM-DD HH:mm:ss'),
    day: current.day(),
    hour: current.hours(),
  };
}

export function getAllComboApplyPrompty(combos) {
  return combos.filter(combo => {
    return combo.condition && combo.condition.products && combo.condition.products.is_all_of;
  });
}

export function getAsiaGifts(product, storeId, quantity, now) {
  let gifts = [];
  if (product && product.promotions) {
    gifts = product.promotions.gift
      .filter(gift => checkGift(gift, storeId, quantity, now))
      .map(gift => ({
        sku: gift.gift,
        altSku: gift.gift_alt,
        quantity: quantity * gift.gift_quantity,
        quantityPerItem: gift.gift_quantity,
      }));
  }

  return gifts;
}

export function isEmptyPromotion(promotion) {
  return !promotion || !promotion.benefit;
}

export function getAsiaPromotion(product, storeId, quantity, now) {
  let gifts = getAsiaGifts(product, storeId, quantity, now);
  let benefit = mapBenefitFromAsiaGifts(gifts);

  return {
    name: 'Sản phẩm tặng kèm (asia)',
    key: 'asia',
    from: 'asia',
    benefit,
  };
}

export function mergeAsiaPromotions(asiaPromotions) {
  if (!asiaPromotions || asiaPromotions.length === 0) return null;

  let allBenefits = asiaPromotions.map(promotion => promotion.benefit);

  return {
    name: 'Sản phẩm tặng kèm (asia)',
    key: 'asia',
    from: 'asia',
    benefit: getAllOfBenefit('', 1, allBenefits),
  };
}

export function checkGift(gift, storeId, quantity, now, ignoreCheckRequiredQuantity = false) {
  // quantity === -1 if this product is from search
  return (
    (gift.branch_code === null || gift.branch_code === storeId) &&
    gift.apply_for != 1 &&
    gift.gift_quantity > 0 &&
    (gift.gift !== '' || gift.gift_alt !== '') &&
    (ignoreCheckRequiredQuantity || quantity === -1 || quantity >= gift.required_quantity) &&
    (now.date > gift.begun_at && now.date < gift.ended_at)
  );
}

export function getAllAgentPromotionsForProduct(product, firebasePromotions) {
  return getAllPromotionsForProduct(product, firebasePromotions, null, 'agent');
}

export function getAllPromotionsForProduct(product, firebasePromotions, storeId, channel) {
  let now = getNow();
  let productPromotions = [];

  // promotions_new
  let productPromotionsNew = getAllPromotionsNewForProduct(product, storeId, now, channel);
  productPromotions = productPromotions.concat(productPromotionsNew);

  // promotions_old
  let promotionOld = getPromotionsOld(product, firebasePromotions, now, null);
  if (promotionOld) {
    productPromotions.push(promotionOld);
  }

  let noPromotion = getNoPromotion();
  productPromotions.push(noPromotion);

  return productPromotions.filter(item => item);
}

export function getNoPromotion() {
  return {
    name: 'Không dùng khuyến mãi',
    key: 'none',
    from: 'none',
    benefit: null,
  };
}

function getAllPromotionsNewForProduct(product, storeId, now, channel) {
  let cartItemTest = {
    sku: product.sku,
    category: product.category,
    quantity: 1,
    price: product.price_w_vat || product.price,
    min_quantity_needed: 1,
  };

  return PromoManager.getProductPromotions(cartItemTest, storeId, now, channel).map(convertProductPromotionNewToLocal);
}

function convertProductPromotionNewToLocal(promotion) {
  let { programKey } = promotion;
  let basePromotionNew = { ...promotion, quantity: 1 };
  let benefit = getBenefitFromPromoNew(basePromotionNew, programKey);
  let updatePath = 'quantity_left' in promotion ? path.getPromotionsNewQuantityLeftPath(programKey, promotion.key) : null;

  return {
    // basePromotionNew,
    uuid: uuidv4(),
    programKey,
    benefit,
    updatePath,
    // products: [promotion.data],
    from: 'promotion_new',
    name: promotion.programName,
    description: promotion.description,
    key: promotion.key,
    total_value: promotion.total_value,
    quantity_left: promotion.quantity_left,
    limited_quantity: promotion.limited_quantity,
  };
}

export function equalPromotion(a, b) {
  if (a == b) return true;
  if (!a || !b) return false;

  return equalByKeys(a, b, 'from', 'key') && equalBenefit(a.benefit, b.benefit);
}

export function getPromotionQuantityLeft(promotionFrom, programKey, promotionKey, { promotionsNew, promotionsOld }) {
  let quantityLeft = null;
  let now = getNow();

  if (promotionFrom === 'promotion_new') {
    let program = getObjectProperty(promotionsNew, programKey);
    if (program && isActiveProgramForAgent(program, now)) {
      quantityLeft = getObjectProperty(promotionsNew, programKey, 'data', promotionKey, 'quantity_left');
    } else {
      quantityLeft = Number.MIN_SAFE_INTEGER;
    }
  } else if (promotionFrom === 'promotion_old') {
    let program = getObjectProperty(promotionsOld, programKey);
    if (program && isValidPromotionOld(program, now)) {
      quantityLeft = getObjectProperty(promotionsOld, programKey, 'data', promotionKey, 'quantity');
    } else {
      quantityLeft = Number.MIN_SAFE_INTEGER;
    }
  }

  if (quantityLeft !== null && !isNaN(quantityLeft)) {
    return quantityLeft;
  } else {
    return Number.MAX_SAFE_INTEGER;
  }
}

export function flatBenefitFromPromotionWithQuantity(promotion, quantity) {
  if (!promotion || promotion.from === 'none' || !promotion.benefit) return [];

  return flatBenefitWithQuantity(promotion.benefit, quantity);
}
