import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import { Util } from 'teko-js-sale-library';

import ImageWrapper from '../../components/ImageWrapper';
import { getAsiaPromotion, getNow } from '../../modules/promotion';
import { fetchProductDetails, fetchProductMagentoDetail } from '../../stores/product/actions';
import { scale } from '../../utils/scaling';
import { getImageSource } from '../../utils/product';
import { colors, textStyles, screen } from '../../resources/styles/common';
import Price from '../common/Price';
import ChangeQuantityItem from '../ChangeQuantityItem';
import Promotion from '../cart/Promotion';
import ProductInfo from '../product/ProductInfo';
import LoadingIndicator from '../common/LoadingIndicator';

export class ProductInComboItem extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { productDetails, item } = nextProps;
    let product = productDetails[item.sku];

    if (product !== prevState.product) {
      return {
        product,
      };
    }

    return null;
  }

  state = {
    product: null,
  };

  componentDidMount() {
    const { item } = this.props;
    const { product } = this.state;
    const productDetail = product && product.detail;

    if (!product || !product.isLoading) {
      if (!productDetail) {
        this.props.fetchProductDetails(item.sku);
      }

      // if (!productDetail || !productDetail.image || !productDetail.attributes) {
      //   this.props.fetchProductMagentoDetail(item.sku);
      // }
    }
  }

  onPressEdit = () => {
    this.props.onPressEdit(this.props.item);
  };

  onPressDelete = () => {
    this.props.onPressDelete(this.props.item);
  };

  onPressItemQuantity = () => {
    this.props.onPressItemQuantity(this.props.item);
  };

  onUpdateProductQuantity = quantity => {
    this.props.onUpdateProductQuantity(quantity, this.props.item);
  };

  renderAsiaPromotion = () => {
    const { asiaPromotion, quantity } = this.props.item;
    const { productDetails, fetchProductDetails } = this.props;

    if (asiaPromotion) {
      return (
        <Promotion
          allowEdit={false}
          promotion={asiaPromotion}
          containerStyle={styles.promotionContainerStyle}
          productQuantity={quantity}
          productDetails={productDetails}
          fetchProductDetails={fetchProductDetails}
        />
      );
    } else {
      return null;
    }
  };

  render() {
    const { product } = this.state;
    const { item, onPressEdit, disabledEdit, onPressDelete, maxQuantity } = this.props;

    let productDetail = product && product.detail;

    if (!product || product.isLoading) {
      return (
        <View style={[styles.emptyContainer, { alignItems: 'center', justifyContent: 'center' }]}>
          <LoadingIndicator />
        </View>
      );
    }

    if (!product.detail) {
      return (
        <View style={styles.emptyContainer}>
          <Text style={textStyles.heading2}>
            {`Không có thông tin cho sản phẩm: `}
            <Text style={{ color: colors.secondary }}>{this.props.item.sku}</Text>
          </Text>
        </View>
      );
    }

    let price = item.price || productDetail.price_w_vat;
    let originalPrice = item.originalPrice || productDetail.original_price;
    let hasPromotionPrice = item.price < item.originalPrice;
    let quantity = item.quantity;

    return (
      <View style={styles.emptyContainer}>
        <View style={{ flex: 1 }}>
          <ProductInfo
            sku={productDetail.sku}
            name={productDetail.name}
            category_code={productDetail.category}
            // image={getImageSource(productDetail)}
            containerStyle={styles.productInfo}
          />
        </View>
        {/* {this.renderAsiaPromotion()} */}
        <View style={{ flexDirection: 'row', padding: scale(12), alignItems: 'center' }}>
          <View style={{ flex: 1 }}>
            <Price style={textStyles.footnote_medium} color={colors.primary} price={price} />
            {hasPromotionPrice ? (
              <Text style={[textStyles.footnote, { textDecorationLine: 'line-through' }]}>
                {Util.Text.numberWithCommas(originalPrice)}
              </Text>
            ) : null}
          </View>
          <View
            style={{
              borderRadius: scale(8),
              borderWidth: scale(1),
              borderColor: colors.lightGray,
              paddingHorizontal: scale(4),
              paddingVertical: scale(1),
            }}
          >
            <Text style={[textStyles.small, { color: colors.black }]}>x{quantity}</Text>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    productDetails: state.product.product_details,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProductDetails: sku => dispatch(fetchProductDetails(sku)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductInComboItem);

ProductInComboItem.propTypes = {
  item: PropTypes.object,
  maxQuantity: PropTypes.number,
};

ProductInComboItem.defaultProps = {
  maxQuantity: 1,
};

const styles = StyleSheet.create({
  emptyContainer: {
    width: scale(165),
    marginLeft: scale(12),
    marginBottom: scale(10),
    height: scale(275),
    backgroundColor: 'white',
  },
  productInfo: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  promotionContainerStyle: {
    marginTop: scale(4),
  },
  button: {
    width: scale(24),
    height: scale(24),
    marginLeft: screen.margin.default,
  },
});
