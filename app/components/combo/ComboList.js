import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, FlatList, ScrollView } from 'react-native';
import { Util } from 'teko-js-sale-library';

import commonStyles, { textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import ComboItem from '../../components/combo/ComboItem';

class ComboList extends Component {
  filterComboPartial(searchText, activeCombos, filterCombo) {
    searchText = Util.Text.convertVNstring(searchText.trim()).toLowerCase();

    activeCombos = activeCombos
      .filter(item => item.searchedName.includes(searchText) || item.searchedDescription.includes(searchText))
      .filter(item => filterCombo(item));

    let canApplyImmediateCombos = activeCombos.filter(combo => combo.isValidCombo);
    let restCombos = activeCombos.filter(combo => !combo.isValidCombo);

    return { canApplyImmediateCombos, restCombos };
  }

  getComboKey = combo => {
    return `${combo.programKey}-${combo.key}`;
  };

  keyExtractor = (item, index) => (item && item.combo && this.getComboKey(item.combo)) || String(index);

  renderComboItem = ({ item }) => {
    let { showBanner } = this.props;
    return (
      <ComboItem
        key={item.combo.key}
        item={item}
        showBanner={showBanner}
        checkable={this.props.checkable}
        selectedCombo={this.props.selectedCombo}
        onPress={this.props.onPressComboItem}
      />
    );
  };

  render = () => {
    let { activeCombos, searchText, filterCombo, shouldShowListHeader } = this.props;
    let { canApplyImmediateCombos, restCombos } = this.filterComboPartial(searchText, activeCombos, filterCombo);

    if (canApplyImmediateCombos.length === 0 && restCombos.length === 0) {
      return (
        <View style={styles.emptyList}>
          <Text style={textStyles.heading1}>Hiện không có combo nào phù hợp</Text>
        </View>
      );
    }

    return (
      <ScrollView style={[commonStyles.full, this.props.containerStyle]}>
        {canApplyImmediateCombos.length > 0 ? (
          <FlatList
            scrollEnabled={false}
            data={canApplyImmediateCombos}
            renderItem={this.renderComboItem}
            keyExtractor={this.keyExtractor}
          />
        ) : null}
        {restCombos.length > 0 ? (
          <FlatList
            scrollEnabled={false}
            data={restCombos}
            renderItem={this.renderComboItem}
            keyExtractor={this.keyExtractor}
          />
        ) : null}
      </ScrollView>
    );
  };
}

export default ComboList;

ComboList.propTypes = {
  selectedCombo: PropTypes.object,
  searchText: PropTypes.string,
  checkable: PropTypes.bool,
  shouldShowListHeader: PropTypes.bool,
  filterCombo: PropTypes.func,
};

ComboList.defaultProps = {
  searchText: '',
  checkable: false,
  shouldShowListHeader: true,
  filterCombo: () => true,
};

const styles = StyleSheet.create({
  emptyList: {
    backgroundColor: 'white',
    height: scale(64),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
