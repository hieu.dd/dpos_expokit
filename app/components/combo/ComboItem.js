import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native';

import { getProductQuantityInCombo } from '../../modules/promotion/combo';
import { scale } from '../../utils/scaling';
import commonStyles, { screen, colors, textStyles } from '../../resources/styles/common';
import CheckButton from '../common/CheckButton';
import ColoredTextLabel from '../label/ColoredTextLabel';

const testImage = 'https://cdn.zeplin.io/5bc40cc5d010065fdf5f728e/assets/32D2B3DF-BDE2-4F60-8E55-F97446714C1D.png';
export class ComboItem extends PureComponent {
  onPress = () => {
    this.props.onPress && this.props.onPress(this.props.item);
  };

  renderWithBanner = () => {
    let promotion = this.props.item.combo;
    return (
      <TouchableOpacity onPress={this.onPress} style={styles.containerBanner}>
        <Image source={{ uri: promotion.banner_url || testImage }} style={styles.banner} />
        <Text style={[textStyles.heading1, { marginHorizontal: screen.margin.smaller, marginTop: screen.margin.smaller }]}>
          {promotion.name}
        </Text>
        <Text
          style={[
            textStyles.subheading,
            { marginHorizontal: screen.margin.smaller, marginBottom: screen.margin.smaller, marginTop: scale(8) },
          ]}
        >
          {promotion.description}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { item, checkable, selectedCombo, showBanner } = this.props;
    let { combo, suggestProducts, comboSize, isValidCombo, isComboNew } = item;
    let nSuggestProducts = isComboNew ? getProductQuantityInCombo(suggestProducts) : suggestProducts.length;
    let text = `${nSuggestProducts}${!isComboNew ? `/${comboSize}` : ''}`;
    if (showBanner) {
      return (
        <View style={{ paddingHorizontal: scale(12), backgroundColor: colors.lightGray, paddingVertical: scale(6) }}>
          {this.renderWithBanner()}
        </View>
      );
    }
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View>
          <View style={styles.container}>
            {checkable ? (
              <CheckButton
                disable={true}
                checked={selectedCombo && selectedCombo.key === combo.key}
                containerStyle={styles.checkButton}
              />
            ) : null}
            <View style={commonStyles.full}>
              <Text style={textStyles.heading1}>{combo.name}</Text>
              {combo.description ? (
                <Fragment>
                  <Text style={textStyles.footnoteRed}>{combo.description}</Text>
                </Fragment>
              ) : null}
            </View>
            {nSuggestProducts > 0 ? (
              <View style={styles.status}>
                <ColoredTextLabel text={text} type={isValidCombo ? 'success' : 'error'} />
              </View>
            ) : null}
          </View>
          <View style={checkable ? styles.checkableDivider : styles.divider} />
        </View>
      </TouchableOpacity>
    );
  }
}

export default ComboItem;

ComboItem.propTypes = {
  item: PropTypes.object,
  checkable: PropTypes.bool,
  selectedCombo: PropTypes.object,
};

ComboItem.defaultProps = {
  checkable: false,
};

const styles = StyleSheet.create({
  container: {
    minHeight: scale(62),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: screen.padding.smaller,
  },
  checkButton: {
    width: scale(20),
    marginLeft: scale(4),
    marginRight: screen.margin.default,
  },
  status: {
    height: '100%',
    paddingTop: scale(6),
    marginLeft: scale(19),
  },
  divider: {
    height: 1,
    backgroundColor: colors.lightGray,
  },
  checkableDivider: {
    height: 1,
    marginHorizontal: screen.margin.smaller,
    backgroundColor: colors.lightGray,
  },
  banner: {
    width: scale(351),
    height: scale(183),
  },
  containerBanner: {
    flex: 1,
    borderRadius: scale(8),
    backgroundColor: 'white',
    overflow: 'hidden',
  },
});
