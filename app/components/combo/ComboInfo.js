import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { checkComboNew } from '../../modules/promotion/combo';
import { scale } from '../../utils/scaling';
import commonStyles, { textStyles, colors, screen } from '../../resources/styles/common';
import Space from '../Space';
import Price from '../common/Price';
import ChangeQuantityItem from '../ChangeQuantityItem';

export class ComboInfo extends PureComponent {
  render() {
    const {
      combo,
      comboPrice,
      quantity,
      quantityLeft,
      hasAlterCombos,
      onPressEdit,
      onUpdateComboQuantity,
      onPressComboQuantity,
    } = this.props;

    let isComboNew = checkComboNew(combo);
    let maxQuantity = isComboNew ? 1 : quantityLeft;
    maxQuantity = Math.min(maxQuantity, 999);

    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row' }}>
          <View style={commonStyles.full}>
            <Text numberOfLines={2} style={textStyles.medium}>
              {combo.name}
            </Text>
            <Text style={[textStyles.body1, { color: colors.primary, marginTop: scale(8) }]}>{combo.description}</Text>
          </View>
          <TouchableOpacity onPress={onPressEdit} disabled={!hasAlterCombos}>
            <View style={styles.editButton}>
              <Icon name="edit" size={scale(22)} color={hasAlterCombos ? colors.gray : colors.lightGray} />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: scale(16) }}>
          <View style={{}}>
            <Text style={textStyles.footnote}>{`Giá trị combo`}</Text>
            <Price price={comboPrice} />
          </View>
          <View style={{ marginLeft: scale(30) }}>
            <ChangeQuantityItem
              editable={!isComboNew}
              quantity={quantity}
              maxQuantity={maxQuantity}
              onUpdateQuantity={onUpdateComboQuantity}
              onPressItemQuantity={onPressComboQuantity}
            />
          </View>
          <View style={commonStyles.full} />
          {quantityLeft !== Number.MAX_SAFE_INTEGER ? (
            <View style={styles.quantityLeft}>
              <Text style={textStyles.subheading}>{`Số lượng: ${quantityLeft}`}</Text>
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}

export default ComboInfo;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: screen.padding.smaller,
  },
  editButton: {
    width: scale(24),
    height: scale(24),
    marginLeft: screen.padding.small,
  },
  quantityLeft: {
    backgroundColor: colors.lightGray,
    height: scale(24),
    borderRadius: scale(8),
    paddingHorizontal: screen.padding.default,
    alignItems: 'center',
    flexDirection: 'row',
  },
});
