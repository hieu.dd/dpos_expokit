import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { checkComboNew } from '../../modules/promotion/combo';
import { scale } from '../../utils/scaling';
import commonStyles, { textStyles, colors, screen } from '../../resources/styles/common';
import Space from '../Space';
import Price from '../common/Price';
import ChangeQuantityItem from '../ChangeQuantityItem';
import TrapezoidButton from '../../components/button/TrapezoidButton';

export class ComboItemInfo extends PureComponent {
  onApplyCombo = () => {
    this.props.applyCombo && this.props.applyCombo();
  };

  _measure = () => {
    this.refPrice.measure((width, height, px, py, fx, fy) => {
      const location = {
        fx: fx,
        fy: fy,
        px: px,
        py: py,
        width: width,
        height: height,
      };
      this.props.getPositionPriceView(location);
    });
  };

  render() {
    const {
      combo,
      comboPrice,
      quantity,
      quantityLeft,
      hasAlterCombos,
      onPressEdit,
      onUpdateComboQuantity,
      onPressComboQuantity,
    } = this.props;

    let isComboNew = checkComboNew(combo);
    let maxQuantity = isComboNew ? 1 : quantityLeft;
    maxQuantity = Math.min(maxQuantity, 999);

    return (
      <View style={styles.container}>
        <View style={{ padding: scale(12) }}>
          <Text style={textStyles.heading1}>{combo.name}</Text>
          <Text style={[textStyles.body1, { color: colors.primary, marginTop: scale(8) }]}>{combo.description}</Text>
        </View>
        <Space height={scale(2)} backgroundColor={colors.lightGray} />
        <View
          collapsable={false}
          ref={view => {
            this.refPrice = view;
          }}
          style={{ flexDirection: 'row', alignItems: 'center', paddingTop: scale(8) }}
        >
          <View
            style={{
              flex: 1,
              // marginTop: scale(8),
              backgroundColor: colors.dark_slate_blue,
              flexDirection: 'row',
              height: scale(40),
              alignItems: 'center',
              paddingVertical: scale(12),
            }}
          >
            <Text style={[textStyles.footnote, { color: 'white' }]}>Thành tiền: </Text>
            <Price color={colors.squash} style={[textStyles.price]} price={comboPrice} />
          </View>
          <View style={{ position: 'absolute', top: 0, right: 0 }}>
            <TrapezoidButton
              color={colors.primary}
              title={'MUA NGAY'}
              textStyle={[textStyles.body1, { color: 'white', fontFamily: 'sale-text-bold' }]}
              width={scale(140)}
              height={scale(48)}
              left={scale(16)}
              iconSize={scale(16)}
              iconRightName="menu-right"
              iconType="material-community"
              iconColor="white"
              onPress={this.onApplyCombo}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default ComboItemInfo;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  orderButton: {
    height: scale(40),
    width: scale(107),
    borderRadius: scale(8),
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
