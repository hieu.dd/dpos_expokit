import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import ImageWrapper from '../components/ImageWrapper';
import { scale } from '../utils/scaling';
import { screen, textStyles } from '../resources/styles/common';
import LoadingIndicator from './common/LoadingIndicator';

class GiftItemInDetail extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    let product = nextProps.product_details[nextProps.gift.sku];

    if (product !== prevState.product) {
      return {
        product,
      };
    }

    return null;
  }

  state = {
    product: null,
  };

  componentDidMount = () => {
    if (!this.state.product) {
      this.props.fetchProductDetails(this.props.gift.sku);
      this.props.fetchProductMagentoDetail(this.props.gift.sku);
    }
  };

  render() {
    const { product } = this.state;
    const { gift } = this.props;

    if (!product || product.isLoading) return <LoadingIndicator />;
    if (!product.detail) return <Text style={{ margin: screen.margin.small }}>Không có thông tin</Text>;
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Text style={textStyles.subheading}>{gift.quantity} x</Text>
        <Text style={[textStyles.body2, { flex: 1, marginLeft: screen.margin.smaller }]}>{product.detail.name}</Text>
        <ImageWrapper
          source={product.detail.image && product.detail.image.base_image_smallsquare}
          style={styles.giftImage}
          indicatorComponent={<LoadingIndicator />}
        />
      </View>
    );
  }
}

export default GiftItemInDetail;

const styles = StyleSheet.create({
  giftImage: {
    width: scale(45),
    height: scale(45),
  },
});
