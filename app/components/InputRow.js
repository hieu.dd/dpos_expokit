import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Platform, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import ModalSelector from '../components/modal/ModalSelector';
import { Icon } from 'react-native-elements';
import { screen, colors } from '../resources/styles/common';
import { Util } from 'teko-js-sale-library';
import { scale } from '../utils/scaling';

export default class InputRow extends Component {
  static propTypes = {
    rowStyle: View.propTypes.style,
    inputStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    captionStyle: PropTypes.object,
    captionTextStyle: PropTypes.object,
    caption: PropTypes.string,
    inputType: PropTypes.oneOf(['textInput', 'numberInput', 'picker', 'textMultiline', 'captionOnly', 'timePicker']),
    hasFindButton: PropTypes.bool,
    inputValue: PropTypes.string,
    onFind: PropTypes.func,
    onEndEditing: PropTypes.func,
    editable: PropTypes.bool,
    onChangeText: PropTypes.func,
    clearTextOnFocus: PropTypes.bool,
    unit: PropTypes.string,
    unitStyle: PropTypes.object,
    unitTextStyle: PropTypes.object,
  };

  static defaultProps = {
    rowStyle: {},
    inputStyle: {},
    captionStyle: {},
    captionTextStyle: {},
    unitStyle: {},
    unitTextStyle: {},
    caption: '',
    inputType: 'textInput',
    hasFindButton: false,
    inputValue: '',
    clearTextOnFocus: false,
    pickerData: [],
    editable: true,
    placeholder: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      // takeValueFromProps: true,
      contentInputOffsetY: 0,
      position: 0,
    };
  }

  onInputFocus() {
    // this.setState({
    //   takeValueFromProps: false
    // });

    if (this.props.clearTextOnFocus) {
      this.props.onChangeText && this.props.onChangeText('');
    }
    this.props.onInputSelected && this.props.onInputSelected(this.state.contentInputOffsetY);
  }

  measureContentInput() {
    this.mainView.measure((frameOffsetX, frameOffsetY, width, height, pageOffsetX, pageOffsetY) => {
      var offsetY = Platform.OS === 'android' && Platform.Version <= 20 ? pageOffsetY : frameOffsetY;
      this.setState({ contentInputOffsetY: offsetY > 0 ? offsetY : 0 });
    });
  }

  onInputBlur() {
    //   this.setState({
    //     takeValueFromProps: true
    //   });
  }

  onLayout = ({
    nativeEvent: {
      layout: { x, y, width, height },
    },
  }) => {
    this.setState(prevState => ({
      position: prevState.position + y,
    }));
  };

  onEditEnd() {
    if (
      this.props.inputType === 'numberInput' &&
      this.props.inputValue &&
      (this.props.inputValue.length === 0 || isNaN(Util.Text.textWithoutCommas(this.props.inputValue)))
    ) {
      this.props.onChangeText && this.props.onChangeText('0');
    }

    if (this.props.onFind && this.props.inputValue) {
      this.props.onFind(this.props.inputValue, true);
    }
  }

  render() {
    let {
      rowStyle,
      inputStyle,
      captionStyle,
      captionTextStyle,
      unitStyle,
      unitTextStyle,
      caption,
      inputType,
      hasFindButton,
      inputValue,
      onFind,
      onEndEditing,
      onChangeText,
      pickerData,
      editable,
      unit,
      accessibilityLabel,
      placeholder,
      rightIcon,
      hintColor,
    } = this.props;

    // let textToDisplay = this.state.takeValueFromProps? this.props.inputValue:this.state.inputValue;

    let renderInput = () => {
      switch (inputType) {
        case 'timePicker':
          return (
            <TouchableOpacity
              onPress={this.props.pickTime}
              style={[styles.pickerInput, inputStyle, { paddingBottom: scale(12) }]}
            >
              <Text style={[styles.defaultInputText, { color: hintColor }]}>{this.props.inputValue}</Text>
            </TouchableOpacity>
          );
        case 'numberInput':
          return (
            <TextInput
              accessibilityLabel={accessibilityLabel}
              keyboardType="numeric"
              style={[hasFindButton ? styles.rowInputWithLookup : styles.rowInput, styles.defaultInputText, inputStyle]}
              onChangeText={text => onChangeText && onChangeText(text)}
              onEndEditing={() => this.onEditEnd()}
              onFocus={() => this.onInputFocus()}
              onBlur={() => this.onInputBlur()}
              value={this.props.inputValue}
              underlineColorAndroid="transparent"
              editable={editable}
              maxLength={20}
              textStyle={unitTextStyle}
              placeholder={placeholder}
              placeholderTextColor={colors.gray}
            />
          );
        case 'picker':
          return (
            <ModalSelector
              ref={ref => (this.modalSelector = ref)}
              accessibilityLabel={accessibilityLabel}
              data={pickerData}
              style={[styles.pickerInput, inputStyle]}
              touchableStyle={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-end' }}
              onChangeText={text => onChangeText && onChangeText(text)}
              selectStyle={{
                borderWidth: 0,
                padding: 0,
                paddingVertical: screen.padding.tiny,
              }}
              selectTextStyle={[styles.defaultInputText, { color: hintColor }]}
              value={this.props.inputValue}
              title={this.props.caption.replace(' *', '')}
              cancelText="Đóng"
              onChange={option => onEndEditing(option.key, option.label)}
              disabled={!editable}
              placeholder={placeholder}
            />
          );
        case 'textMultiline':
          return (
            <TextInput
              accessibilityLabel={accessibilityLabel}
              style={[
                hasFindButton ? styles.rowInputWithLookup : styles.rowInput,
                styles.defaultInputText,
                {
                  textAlignVertical: 'top',
                },
                inputStyle,
              ]}
              onChangeText={text => onChangeText && onChangeText(text)}
              // onEndEditing={() => onEndEditing(this.state.inputValue)}
              onFocus={() => this.onInputFocus()}
              onBlur={() => this.onInputBlur()}
              value={this.props.inputValue}
              underlineColorAndroid="transparent"
              editable={editable}
              maxLength={255}
              placeholder={placeholder}
              placeholderTextColor={colors.gray}
              multiline
            />
          );
        case 'captionOnly':
          return null;
        case 'textInput':
        default:
          return (
            <TextInput
              accessibilityLabel={accessibilityLabel}
              style={[hasFindButton ? styles.rowInputWithLookup : styles.rowInput, styles.defaultInputText, inputStyle]}
              onChangeText={text => onChangeText && onChangeText(text)}
              // onEndEditing={() => onEndEditing(this.state.inputValue)}
              onFocus={() => this.onInputFocus()}
              onBlur={() => this.onInputBlur()}
              value={this.props.inputValue}
              placeholder={placeholder}
              placeholderTextColor={colors.gray}
              underlineColorAndroid="transparent"
              editable={editable}
              maxLength={255}
            />
          );
      }
    };

    let styleWhenUneditable = editable
      ? null
      : {
          backgroundColor: colors.lightGray,
          borderBottomColor: 'transparent',
          // marginHorizontal: -screen.padding.smaller,
          // paddingHorizontal: screen.padding.smaller
        };

    if (rightIcon) {
      if (inputType === 'picker' || inputType === 'timePicker') {
        rightIcon = (
          <TouchableOpacity
            style={{ flex: 1, justifyContent: 'center' }}
            onPress={this.props.pickTime || (this.modalSelector && this.modalSelector.onPress)}
          >
            {rightIcon}
          </TouchableOpacity>
        );
      } else {
        rightIcon = <View style={{ flex: 1, justifyContent: 'center' }}>{rightIcon}</View>;
      }
    }
    return (
      <View
        style={[
          this.props.heightCustomerWork ? styles.contentRowCus : styles.contentRow,
          styleWhenUneditable,
          inputType === 'textMultiline' ? { height: -1 } : null,
          rowStyle,
        ]}
        ref={ref => {
          this.mainView = ref;
        }}
        onLayout={this.onLayout}
      >
        {caption ? (
          <View style={[styles.rowCaption, captionStyle]}>
            <Text style={[styles.captionTextStyle, captionTextStyle]} accessibilityLabel="input_row_caption">
              {caption}
            </Text>
          </View>
        ) : null}
        {renderInput()}
        {hasFindButton ? (
          <TouchableOpacity
            style={styles.rowLookupButton}
            onPress={() => {
              onFind && inputValue ? onFind(inputValue, true) : {};
            }}
            disabled={!this.props.editable}
          >
            <Icon name={'search'} size={25} color="white" />
          </TouchableOpacity>
        ) : null}
        {unit ? (
          <View style={[styles.rowCaption, unitStyle]}>
            <Text style={[styles.captionTextStyle, unitTextStyle]}>{unit}</Text>
          </View>
        ) : null}
        {rightIcon}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentRow: {
    flexDirection: 'row',
    borderBottomColor: colors.lightGray,
    borderBottomWidth: 1,
    marginBottom: screen.margin.smaller,
    height: scale(44),
  },
  contentRowCus: {
    flexDirection: 'row',
    height: scale(19),
  },
  rowCaption: {
    flex: 4,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
    paddingBottom: screen.padding.smaller,
  },
  rowInput: {
    flex: 6,
    flexDirection: 'row',
    // borderBottomWidth: 1,
    // borderBottomColor: 'lightgray'
  },
  rowInputWithLookup: {
    flex: 4,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 2,
    margin: 5,
    padding: 2,
    paddingLeft: 5,
  },
  defaultInputText: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(15),
  },
  pickerInput: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  rowLookupButton: {
    flex: 2,
    alignItems: 'center',
    backgroundColor: '#28384b',
    margin: 5,
    borderWidth: 1,
    borderRadius: 2,
    justifyContent: 'center',
  },
  captionTextStyle: {
    color: 'rgb(38, 40, 41)',
    fontSize: screen.fontSize.medium,
    fontFamily: 'sale-text-regular',
  },
});
