import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { colors, textStyles } from '../../resources/styles/common';
import { scale, getLetterSpacing } from '../../utils/scaling';
import Space from '../Space';

export class PromotionHeader extends PureComponent {
  getPromotionHint(promotion) {
    const { benefit } = promotion;
    let hint = null;
    if (benefit && benefit.childrenType === 'oneOf') {
      hint = `Bấm để chọn 1 trong ${benefit.children.length} khuyến mãi:`;
    }

    return hint;
  }

  render() {
    const { expanded, onToggleExpand, promotion } = this.props;
    const { name, description, quantity_left } = promotion;
    let promotionHint = this.getPromotionHint(promotion);

    return (
      <View style={styles.header}>
        <TouchableOpacity onPress={onToggleExpand}>
          <View style={{ flexDirection: 'row', minHeight: scale(24), alignItems: 'flex-start' }}>
            <View style={styles.left}>
              <Image source={require('../../resources/images/promotionFilled.png')} style={styles.promotionImage} />
            </View>
            <View style={styles.center}>
              <Text numberOfLines={2} style={textStyles.heading2}>
                {name}
              </Text>
              {quantity_left ? (
                <Fragment>
                  <Space height={scale(4)} />
                  <Text style={styles.quantityLeft}>{`Số lượng KM: ${quantity_left}`}</Text>
                </Fragment>
              ) : null}
            </View>
            <View>
              <Icon
                name={expanded ? 'chevron-up' : 'chevron-down'}
                type="material-community"
                color={colors.gray}
                size={scale(24)}
                containerStyle={{ width: scale(32), height: scale(24), justifyContent: 'center', alignItems: 'center' }}
              />
            </View>
          </View>
        </TouchableOpacity>
        {expanded && description ? (
          <Fragment>
            <Space height={scale(4)} />
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={styles.left} />
              <View style={styles.center}>
                <Text numberOfLines={2} style={textStyles.subheading}>
                  {description}
                </Text>
                {promotionHint ? (
                  <Text numberOfLines={2} style={textStyles.subheading}>
                    {promotionHint}
                  </Text>
                ) : null}
              </View>
              <View style={styles.right} />
            </View>
          </Fragment>
        ) : null}
      </View>
    );
  }
}

export default PromotionHeader;

PromotionHeader.propTypes = {
  expanded: PropTypes.bool,
  promotion: PropTypes.object,
  onToggleExpand: PropTypes.func,
};

const styles = StyleSheet.create({
  header: {},
  left: {
    width: scale(36),
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scale(6),
  },
  center: {
    flex: 1,
    paddingHorizontal: scale(6),
  },
  right: {
    backgroundColor: 'blue',
    paddingHorizontal: scale(6),
  },
  promotionImage: {
    width: scale(24),
    height: scale(24),
  },
  quantityLeft: {
    fontFamily: 'sale-text-regular',
    fontSize: scale(13),
    lineHeight: scale(18),
    letterSpacing: getLetterSpacing(-0.08),
    textAlignVertical: 'center',
    color: colors.primary,
  },
});
