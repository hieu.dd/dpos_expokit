import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import { fetchProductDetails } from '../../../stores/product/actions';
import Item from './Item';
import Price from '../../common/Price';
import { scale } from '../../../utils/scaling';
import { textStyles, colors } from '../../../resources/styles/common';

export class GiftItem extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { productDetails, benefit } = nextProps;
    let gift = productDetails[benefit.value.gift.sku];
    let isLoading = !!(!gift || gift.isLoading);

    return {
      gift,
      isLoading,
    };
  }

  state = {
    isLoading: false,
    gift: null,
  };

  componentDidMount() {
    const { benefit } = this.props;
    const { gift } = this.state;
    if (!gift) {
      this.props.fetchProductDetails(benefit.value.gift.sku);
    }
  }

  render() {
    const { isLoading, gift } = this.state;
    const { benefit } = this.props;
    let quantity = benefit.value.gift.quantity || 0;
    let text = `${(gift && gift.detail && gift.detail.name) || 'Không có thông tin'}`;
    if (!gift || !gift.detail) {
      return <Item isLoading={isLoading} quantity={quantity} text={text} />;
    } else {
      return (
        <View
          style={{
            padding: scale(8),
            borderBottomColor: colors.lightGray,
            borderBottomWidth: scale(1),
            flexDirection: 'row',
            width: scale(340),
          }}
        >
          <Image source={require('../../../resources/images/no_product.png')} style={{ width: scale(60), height: scale(60) }} />
          <View style={{ flex: 1, height: scale(60), marginLeft: scale(8) }}>
            <Text style={[textStyles.footnote, { color: colors.black }]}>{gift.detail.name}</Text>
            <Price
              price={gift.detail.original_price}
              style={[textStyles.footnote, { fontFamily: 'sale-text-medium', marginTop: scale(4) }]}
            />
          </View>
          <View style={{ justifyContent: 'flex-end', width: scale(22) }}>
            <View
              style={{
                width: scale(22),
                height: scale(16),
                borderRadius: scale(8),
                borderWidth: scale(1),
                borderColor: colors.lightGray,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text>x{quantity}</Text>
            </View>
          </View>
        </View>
      );
    }
  }
}

GiftItem.propTypes = {
  benefit: PropTypes.object,
  productDetails: PropTypes.object,
  fetchProductDetails: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    productDetails: state.product.product_details,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProductDetails: sku => dispatch(fetchProductDetails(sku)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GiftItem);
