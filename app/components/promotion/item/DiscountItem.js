import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';

import { scale } from '../../utils/scaling';
import { screen, textStyles } from '../../resources/styles/common';

export class DiscountItem extends PureComponent {
  getBenefitQuantity = benefit => {
    let quantity = benefit.quantity || 1;

    return quantity;
  };

  render() {
    const { quantity, text } = this.props;

    return (
      <View style={styles.container}>
        <Text style={textStyles.subheading}>{`${quantity} x`}</Text>
        <View style={{ flex: 1, marginLeft: screen.margin.smaller }}>
          <Text numberOfLines={2} textBreakStrategy={'balanced'} style={textStyles.body2}>
            {text}
          </Text>
        </View>
      </View>
    );
  }
}

export default DiscountItem;

DiscountItem.propTypes = {
  quantity: PropTypes.number,
  text: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    minHeight: scale(40),
    paddingTop: scale(11),
    paddingBottom: scale(10),
  },
});
