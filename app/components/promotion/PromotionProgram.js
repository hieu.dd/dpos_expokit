import React, { PureComponent, Fragment } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

import { changeOneAllOfBenefitQuantity, flatBenefit } from '../../modules/promotion/benefit';
import { colors, textStyles, screen } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import PromotionHeader from './PromotionHeader';
import CheckButton from '../common/CheckButton';
import FlattedBenefits from './FlattedBenefits';
import ThreeLevelBenefit from './ThreeLevelBenefit';

export class ProductPromotions extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { promotion, selectedPromotion } = nextProps;
    if (selectedPromotion && promotion.from === selectedPromotion.from && promotion.key === selectedPromotion.key) {
      return {
        promotion: selectedPromotion,
        benefit: selectedPromotion.benefit,
        flattedBenefits: selectedPromotion.benefit && flatBenefit(selectedPromotion.benefit),
      };
    }

    if (promotion !== prevState.promotion) {
      return {
        promotion: promotion,
        benefit: promotion.benefit,
        flattedBenefits: promotion.benefit && flatBenefit(promotion.benefit),
      };
    }

    return null;
  }

  state = {
    expanded: true,
    benefit: null,
    flattedBenefits: null,
  };

  onToggleExpand = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  onChangeBenefit = path => {
    let newBenefit = changeOneAllOfBenefitQuantity(this.state.benefit, path);
    if (newBenefit !== this.state.benefit) {
      // this.setState({ benefit: newBenefit, flattedBenefits: flatBenefit(newBenefit) });
      this.props.onChangeSelectedPromotion &&
        this.props.onChangeSelectedPromotion({ ...this.state.promotion, benefit: newBenefit });
    }
  };

  renderBenefit = () => {
    if (!this.state.benefit) return null;

    if (this.state.expanded) {
      return (
        <View style={styles.benefitContainer}>
          <ThreeLevelBenefit benefit={this.state.benefit} onChangeBenefit={this.onChangeBenefit} />
        </View>
      );
    } else if (this.state.flattedBenefits && this.state.flattedBenefits.length > 0) {
      return (
        <View style={styles.benefitContainer}>
          <FlattedBenefits flattedBenefits={this.state.flattedBenefits} />
        </View>
      );
    }

    return null;
  };

  onSelectNoPromotion = () => {
    this.props.onChangeSelectedPromotion({ ...this.state.promotion });
  };

  renderNoPromotion = () => {
    const { selectedPromotion } = this.props;
    let hasSelectedNoPromotion = selectedPromotion && selectedPromotion.from === 'none';

    return (
      <TouchableOpacity onPress={this.onSelectNoPromotion}>
        <View style={styles.noPromotionContainer}>
          <CheckButton
            checked={hasSelectedNoPromotion}
            disabled={true}
            size={scale(24)}
            containerStyle={styles.noPromotionCheckButton}
          />
          <Text style={textStyles.heading2}>{'Không chọn khuyến mãi'}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    if (this.state.promotion.from === 'none') {
      return null;
    }

    return (
      <View>{this.renderBenefit()}</View>
      // <View style={this.state.expanded ? styles.expandedContainer : styles.collapsedContainer}>
      //   <PromotionHeader promotion={this.props.promotion} expanded={this.state.expanded} onToggleExpand={this.onToggleExpand} />
      //   {this.renderBenefit()}
      // </View>
    );
  }
}

export default ProductPromotions;

const styles = StyleSheet.create({
  expandedContainer: {
    backgroundColor: colors.lightGray,
    borderRadius: scale(8),
    padding: scale(6),
    paddingVertical: scale(12),
    borderWidth: 1,
    borderColor: colors.lightGray,
  },
  collapsedContainer: {
    borderRadius: scale(8),
    padding: scale(6),
    paddingVertical: scale(12),
    borderWidth: 1,
    borderColor: colors.lightGray,
  },
  noPromotionContainer: {
    flexDirection: 'row',
    height: scale(48),
    alignItems: 'center',
    backgroundColor: 'white',
    padding: scale(6),
    paddingVertical: scale(12),
    borderRadius: scale(8),
    borderWidth: 1,
    borderColor: colors.lightGray,
  },
  noPromotionCheckButton: {
    width: scale(36),
    marginRight: scale(6),
  },
  benefitContainer: {
    // marginHorizontal: scale(6),
    // marginTop: screen.margin.smaller,
  },
});
