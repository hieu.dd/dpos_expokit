import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet } from 'react-native';
import { cloneDeep } from 'lodash';

import { getAllAgentPromotionsForProduct } from '../../modules/promotion';
import { changeBenefitQuantity } from '../../modules/promotion/benefit';
import { screen, textStyles, colors } from '../../resources/styles/common';
import PromotionProgram from '../promotion/PromotionProgram';
import TitleLabel from '../label/TitleLabel';

export class ProductPromotions extends PureComponent {
  state = {
    hasPromotions: false,
    allPromotionsForProduct: [],
    selectedPromotion: null,
  };

  componentDidMount = () => {
    this.checkPromotions();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (
      (this.props.product && this.props.product !== prevProps.product) ||
      this.props.promotionsOld !== prevProps.promotionsOld
    ) {
      this.checkPromotions();
    }
  };

  reload = () => {
    this.checkPromotions();
  };

  getSelectedPromotion = () => {
    return this.state.selectedPromotion;
  };

  checkPromotions = () => {
    const { product, promotionsOld } = this.props;
    let { selectedPromotion } = this.state;

    let allPromotionsForProduct = getAllAgentPromotionsForProduct(product, promotionsOld);
    let existedPromotion = selectedPromotion ? allPromotionsForProduct.find(item => item.key === selectedPromotion.key) : null;

    if (existedPromotion) {
      selectedPromotion = cloneDeep(existedPromotion);
      selectedPromotion.benefit = changeBenefitQuantity(selectedPromotion.benefit, '', 1);
    } else {
      selectedPromotion = allPromotionsForProduct.length > 0 ? cloneDeep(allPromotionsForProduct[0]) : null;
      selectedPromotion.benefit = changeBenefitQuantity(selectedPromotion.benefit, '', 1);
    }

    let hasPromotions = allPromotionsForProduct.some(promotion => promotion.from !== 'none');
    this.props.changeProductPromotion(selectedPromotion);
    this.setState({ allPromotionsForProduct, selectedPromotion, hasPromotions });
  };

  onChangeSelectedPromotion = promotion => {
    this.setState({ selectedPromotion: promotion });
    this.props.changeProductPromotion(promotion);
  };

  render() {
    if (this.state.hasPromotions) {
      return (
        <View style={styles.container}>
          <TitleLabel text={'Chương trình khuyến mãi'} />
          {this.state.allPromotionsForProduct.map((promotion, index) => (
            <View key={promotion.from + (promotion.key || String(index))}>
              <PromotionProgram
                promotion={promotion}
                selectedPromotion={this.state.selectedPromotion}
                onChangeSelectedPromotion={this.onChangeSelectedPromotion}
              />
            </View>
          ))}
        </View>
      );
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  return {
    promotionsOld: state.firebase.promotionsOld,
  };
}

export default connect(
  mapStateToProps,
  null,
  null,
  { withRef: true }
)(ProductPromotions);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white_two,
  },
});
