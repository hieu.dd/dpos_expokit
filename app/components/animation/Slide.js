import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';

export default class Slide extends PureComponent {
  constructor(props) {
    super(props);

    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    if (this.props.show) {
      this.animatedValue.setValue(1);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { duration } = prevProps;

    if (this.props.show && !prevProps.show) {
      this.animatedValue.setValue(0);
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration,
        useNativeDriver: true,
      }).start();
    } else if (!this.props.show && prevProps.show) {
      this.animatedValue.setValue(1);
      Animated.timing(this.animatedValue, {
        toValue: 0,
        duration,
        useNativeDriver: true,
      }).start();
    }
  }

  render() {
    const { children, delta, style } = this.props;

    return (
      <Animated.View
        style={[
          style,
          {
            transform: [
              {
                translateY: this.animatedValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [delta, 0],
                }),
              },
            ],
          },
        ]}
      >
        {children}
      </Animated.View>
    );
  }
}

Slide.propTypes = {
  show: PropTypes.bool,
  delta: PropTypes.number.isRequired,
  duration: PropTypes.number,
};

Slide.defaultProps = {
  duration: 200,
};
