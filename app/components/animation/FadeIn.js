import React, { PureComponent } from 'react';
import { Animated } from 'react-native';

export default class FadeIn extends PureComponent {
  constructor(props) {
    super(props);

    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    if (this.props.show) {
      this.animatedValue.setValue(1);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.show && !prevProps.show) {
      this.animatedValue.setValue(0);
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 500,
      }).start();
    } else if (!this.props.show && prevProps.show) {
      this.animatedValue.setValue(1);
      Animated.timing(this.animatedValue, {
        toValue: 0,
        duration: 500,
      }).start();
    }
  }

  render() {
    const { children } = this.props;

    return <Animated.View style={{ opacity: this.animatedValue }}>{children}</Animated.View>;
  }
}
