import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Util } from 'teko-js-sale-library';
import { Icon } from 'react-native-elements';

import { flatBenefit, getFlattedBenefitsWithQuantity } from '../../modules/promotion/benefit';
import { scale, getLetterSpacing } from '../../utils/scaling';
import commonStyles, { colors, screen } from '../../resources/styles/common';
import Space from '../Space';
import GiftItem from './GiftItem';
import PromotionItem from './PromotionItem';

export class Promotion extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { promotion, productQuantity } = nextProps;
    let { flattedBenefits } = prevState;

    if (promotion !== prevState.promotion) {
      flattedBenefits = (promotion && flatBenefit(promotion.benefit)) || [];
    }

    if (flattedBenefits !== prevState.flattedBenefits || productQuantity !== prevState.productQuantity) {
      let flattedBenefitsWithQuantity = getFlattedBenefitsWithQuantity(flattedBenefits, productQuantity);

      return { promotion, productQuantity, flattedBenefits, flattedBenefitsWithQuantity };
    }

    return null;
  }

  state = {
    productQuantity: 0,
    promotion: null,
    flattedBenefits: [],
    flattedBenefitsWithQuantity: [],
  };

  renderPromotion = () => {
    const { flattedBenefitsWithQuantity } = this.state;

    return (
      <View>
        {flattedBenefitsWithQuantity.map((benefit, index) => {
          if (benefit.sku) {
            return (
              <GiftItem
                key={String(index)}
                item={benefit}
                hasBorderTop={index > 0}
                productDetails={this.props.productDetails}
                fetchProductDetails={this.props.fetchProductDetails}
              />
            );
          } else if (benefit.discount_percentage) {
            return (
              <PromotionItem
                key={String(index)}
                hasBorderTop={index > 0}
                quantity={benefit.quantity}
                text={`Giảm giá ${benefit.discount_percentage}%`}
              />
            );
          } else if (benefit.promotion_discount) {
            return (
              <PromotionItem
                key={String(index)}
                hasBorderTop={index > 0}
                quantity={benefit.quantity}
                text={`Giảm giá ${Util.Text.formatPrice(benefit.promotion_discount)}`}
              />
            );
          } else if (benefit.grand_discount) {
            return (
              <PromotionItem
                key={String(index)}
                hasBorderTop={index > 0}
                quantity={benefit.quantity}
                text={`Giảm giá toàn đơn ${Util.Text.formatPrice(benefit.grand_discount)}`}
              />
            );
          } else if (benefit.promotion_discount) {
            return (
              <PromotionItem
                key={String(index)}
                hasBorderTop={index > 0}
                quantity={benefit.quantity}
                text={`Giảm giá toàn đơn ${Util.Text.formatPrice(benefit.promotion_discount)}`}
              />
            );
          } else if (benefit.promotion_price) {
            return (
              <PromotionItem
                key={String(index)}
                hasBorderTop={index > 0}
                quantity={benefit.quantity}
                text={`Giá bán khuyến mãi ${Util.Text.formatPrice(benefit.promotion_price)}`}
              />
            );
          } else if (benefit.voucher) {
            return (
              <PromotionItem
                key={String(index)}
                hasBorderTop={index > 0}
                quantity={benefit.quantity}
                text={benefit.voucher.name}
              />
            );
          } else {
            return null;
          }
        })}
      </View>
    );
  };

  render() {
    const { promotion, allowEdit, containerStyle } = this.props;
    const { flattedBenefitsWithQuantity } = this.state;
    if (!promotion || promotion.from === 'none' || flattedBenefitsWithQuantity.length === 0) return null;

    return (
      <View style={[styles.container, containerStyle]}>
        <View style={styles.header}>
          <Image source={require('../../resources/images/present.png')} style={styles.giftImage} />
          <Space width={scale(4)} />
          <View style={commonStyles.full}>
            <Text style={styles.programName}>{promotion.name}</Text>
          </View>
          {allowEdit ? (
            <TouchableOpacity onPress={this.props.onEditPromotion}>
              <View style={styles.button}>
                <Icon name="edit" color={colors.gray} size={scale(20)} />
              </View>
            </TouchableOpacity>
          ) : null}
        </View>
        <View>{this.renderPromotion()}</View>
      </View>
    );
  }
}

export default Promotion;

Promotion.propTypes = {
  allowEdit: PropTypes.bool,
  promotion: PropTypes.object,
  containerStyle: View.propTypes.style,
};

Promotion.defaultProps = {
  allowEdit: true,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGray,
    borderRadius: scale(8),
  },
  header: {
    flexDirection: 'row',
    paddingTop: screen.padding.default,
    paddingLeft: screen.padding.smaller,
    paddingRight: screen.padding.default,
  },
  giftImage: {
    width: scale(16),
    height: scale(16),
    marginTop: scale(1),
  },
  programName: {
    height: scale(18),
    fontFamily: 'sale-text-medium',
    fontSize: scale(13),
    lineHeight: scale(18),
    justifyContent: 'center',
    textAlignVertical: 'center',
    letterSpacing: getLetterSpacing(-0.2),
    color: colors.primary,
  },
});
