import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, Text, View } from 'react-native';

import header_styles from '../../resources/styles/header';
import { colors } from '../../resources/styles/common';
import CustomStatusBar from './CustomStatusBar';
import { LinearGradient } from 'expo';
import { scale } from '../../utils/scaling';

export class Header extends Component {
  constructor(props) {
    super(props);
  }

  renderTitle() {
    if (this.props.title) {
      return (
        <Text accessibilityLabel="header_title" style={[header_styles.titleText, this.props.titleTextStyle]} numberOfLines={1}>
          {this.props.title}
        </Text>
      );
    } else {
      return null;
    }
  }

  renderLeftSection() {
    return this.props.leftSection || <View style={header_styles.iconPlaceholder} />;
  }

  renderRightSection() {
    return this.props.rightSection || null;
  }

  renderContent = () => {
    return (
      <View style={[header_styles.header]}>
        <View style={header_styles.title}>{this.renderTitle()}</View>
        <View
          style={{
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {this.renderLeftSection()}
        </View>
        <View
          style={{
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          {this.renderRightSection()}
        </View>
      </View>
    );
  };

  render() {
    const { gradient, color, statusBar, colors } = this.props;

    return (
      <View>
        {gradient ? (
          <LinearGradient start={[0, 1]} end={[1, 1]} colors={colors} style={header_styles.container}>
            {this.renderContent()}
          </LinearGradient>
        ) : (
          <View style={[header_styles.container, { backgroundColor: color }]}>{this.renderContent()}</View>
        )}
      </View>
    );
  }
}

Header.propTypes = {
  gradient: PropTypes.bool,
  title: PropTypes.string,
  color: PropTypes.string,
  statusBar: PropTypes.object,
};

Header.defaultProps = {
  gradient: true,
  title: '',
  titleTextStyle: {},
  color: colors.primary,
  colors: [colors.tomato, colors.primary],
  statusBar: {
    barStyle: 'light-content',
  },
};

export default Header;
