import React from 'react';
import { View, Text, Image } from 'react-native';
import TrapezoidButton from '../button/TrapezoidButton';
import Price from '../common/Price';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';

export const DposHeader = ({ onPress }) => {
  return (
    <View
      style={{
        height: scale(50),
        backgroundColor: colors.primary,
        flexDirection: 'row',
        alignItems: 'center',
        elevation: 5,
      }}
    >
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <TrapezoidButton
          color={colors.gold}
          width={scale(70)}
          height={scale(50)}
          right={scale(12)}
          iconSize={scale(24)}
          iconLeftName="chevron-left"
          iconType="material"
          iconColor="black"
          onPress={onPress}
        />
      </View>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image
          ref={image => {
            this._image = image;
          }}
          style={{ width: scale(24), height: scale(24) }}
          source={require('../../resources/images/logo_phong_vu.png')}
        />
      </View>
      <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', paddingRight: scale(12) }}>
        {/* <Icon name="home" size={scale(24)} type={'material-commnuity'} color="white" /> */}
      </View>
    </View>
  );
};
