import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { screen } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';

export default class CustomHeaderIcon extends PureComponent {
  render() {
    const { name, type, size, color, accessibilityLabel, onPress, style } = this.props;

    return (
      <TouchableOpacity accessibilityLabel={accessibilityLabel} style={[styles.iconPlaceholder, style]} onPress={onPress}>
        <Icon type={type} name={name} size={size} color={color} />
      </TouchableOpacity>
    );
  }
}

CustomHeaderIcon.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string,
  color: PropTypes.string,
  accessibilityLabel: PropTypes.string,
  onPress: PropTypes.func,
  size: PropTypes.number,
};

CustomHeaderIcon.defaultProps = {
  size: screen.iconSize.default,
  type: 'material',
  name: 'chevron-left',
  color: 'white',
  accessibilityLabel: '',
  onPress: () => null,
};

const styles = StyleSheet.create({
  iconPlaceholder: {
    backgroundColor: 'transparent',
    width: scale(36),
    height: scale(36),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
