import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, StatusBar } from 'react-native';

import { colors, screen } from '../../resources/styles/common';
import { LinearGradient } from 'expo';

export default class CustomStatusBar extends PureComponent {
  render() {
    const { gradient, backgroundColor, colors, barStyle } = this.props;

    if (gradient) {
      return (
        <LinearGradient start={[0, 1]} end={[1, 1]} colors={colors} style={{ height: screen.header.statusBarHeight }}>
          <StatusBar barStyle={barStyle} />
        </LinearGradient>
      );
    } else {
      return (
        <View style={{ height: screen.header.statusBarHeight, backgroundColor }}>
          <StatusBar barStyle={barStyle} />
        </View>
      );
    }
  }
}

CustomStatusBar.propTypes = {
  gradient: PropTypes.bool,
  barStyle: PropTypes.string,
};

CustomStatusBar.defaultProps = {
  gradient: true,
  backgroundColor: colors.primary,
  barStyle: 'light-content',
  colors: ['white', 'white'],
};
