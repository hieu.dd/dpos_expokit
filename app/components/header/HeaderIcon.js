import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { screen, colors } from '../../resources/styles/common';
import headerStyles from '../../resources/styles/header';
import { scale, getLetterSpacing } from '../../utils/scaling';

export default class HeaderIcon extends PureComponent {
  render() {
    const { name, type, size, color, accessibilityLabel, onPress, style, badgeText } = this.props;

    return (
      <TouchableOpacity accessibilityLabel={accessibilityLabel} onPress={onPress}>
        <View style={[headerStyles.iconPlaceholder, style]}>
          <Icon type={type} name={name} size={size} color={color} />
          {badgeText ? (
            <View style={styles.badge}>
              <Text style={styles.badgeText}>{badgeText}</Text>
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  }
}

HeaderIcon.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string,
  color: PropTypes.string,
  accessibilityLabel: PropTypes.string,
  onPress: PropTypes.func,
  size: PropTypes.number,
};

HeaderIcon.defaultProps = {
  size: screen.iconSize.fw,
  type: 'material',
  name: 'chevron-left',
  color: 'white',
  accessibilityLabel: '',
  onPress: () => null,
};

const styles = StyleSheet.create({
  badge: {
    position: 'absolute',
    top: scale(4),
    right: scale(6),
    height: scale(16),
    minWidth: scale(16),
    paddingHorizontal: scale(4),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.pumpkinOrange,
    borderRadius: scale(12),
  },
  badgeText: {
    color: 'white',
    fontSize: scale(10),
    letterSpacing: getLetterSpacing(-0.24),
    fontFamily: 'sale-text-medium',
  },
});
