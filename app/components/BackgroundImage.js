import React, { Component } from 'react';
import { View, Image, Animated, Easing } from 'react-native';
import { colors, screen } from '../resources/styles/common';
import { scale } from '../utils/scaling';

export default class BackgroundImage extends Component {
  constructor() {
    super();
    this.animatedValue = new Animated.Value(0);
  }
  componentDidMount() {
    // this.animate();
  }
  state = { uri: null, position: null };

  setUri = (uri, position) => {
    if (position !== this.state.position) {
      this.animatedValue.setValue(0.4);
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 1000,
        // easing: Easing.linear,
      }).start(() => this.setState({ uri, position }));
    }
  };
  render() {
    const opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 1],
    });
    return (
      <View style={{ flex: 1 }}>
        <Animated.Image
          blurRadius={25}
          source={{ uri: this.state.uri || this.props.initUri }}
          style={[
            {
              width: screen.width,

              height: screen.height,
            },
            this.state.uri ? { opacity } : {},
          ]}
        />

        <View
          style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.4)',
            alignItems: 'center',
          }}
        >
          <Image
            style={{ marginVertical: scale(13), width: scale(24), height: scale(24) }}
            source={require('../resources/images/logo_phong_vu.png')}
          />
          <View style={{ width: screen.width, height: 1, backgroundColor: colors.lightGray }} />
        </View>
      </View>
    );
  }
}
