import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { DangerZone } from 'expo';
import { Text, View, StyleSheet, Modal, ScrollView, TouchableWithoutFeedback } from 'react-native';

import { colors, textStyles, screen } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import Space from '../Space';
import ModalButtons from '../modal/ModalButtons';

const { Lottie } = DangerZone;

const ANIMATIONS = {
  loading: { loop: true, source: require('../../resources/animations/loading/loading-small-red.json') },
  success: { loop: false, source: require('../../resources/animations/success.json') },
  error: { loop: false, source: require('../../resources/animations/error.json') },
};

const defaultActions = [{ text: 'Đóng' }];

class AnimationAlertModal extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (!nextProps.visible) {
      return { showDetail: false };
    }

    return null;
  }

  state = {
    showDetail: false,
  };

  componentDidMount = () => {
    if (this.props.visible) {
      this.animationRef && this.animationRef.play();
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (!prevProps.visible && this.props.visible) {
      this.animationRef && this.animationRef.play();
    }

    if (this.props.visible && this.props.type !== prevProps.type) {
      this.animationRef && this.animationRef.play();
    }
  };

  onRequestClose = () => {};

  handleAnimationRef = ref => {
    this.animationRef = ref;
  };

  onToggleDetail = () => {
    this.setState({ showDetail: !this.state.showDetail });
  };

  renderActions = () => {
    let { actions } = this.props;
    if (!actions || actions.length === 0) {
      actions = defaultActions;
    }
    actions = this.addShowDetailAction(actions);
    actions = actions.map(action => ({
      ...action,
      onPress: () => {
        !action.shouldNotClose && this.onClose();
        action.onPress && action.onPress();
      },
    }));

    return <ModalButtons actions={actions} />;
  };

  addShowDetailAction = actions => {
    if (this.props.detail) {
      actions = [
        { text: this.state.showDetail ? 'Ẩn' : 'Chi tiết', onPress: this.onToggleDetail, shouldNotClose: true },
        ...actions,
      ];
    }

    return actions;
  };

  onClose = () => {
    this.props.onClose();
  };

  renderDetail = () => {
    const { detail } = this.props;

    if (!detail || !this.state.showDetail) return null;

    return (
      <View style={styles.detailContainer}>
        <ScrollView>
          <Text>{detail}</Text>
        </ScrollView>
      </View>
    );
  };

  render() {
    const { visible, animationType, message, title } = this.props;
    const animation = ANIMATIONS[animationType] || ANIMATIONS.loading;

    return (
      <Modal visible={visible} onRequestClose={this.onRequestClose} animationType="none" transparent={true}>
        <TouchableWithoutFeedback>
          <View style={styles.container}>
            <View style={{ borderRadius: scale(18), backgroundColor: 'white' }}>
              <View style={[styles.body]}>
                <View>
                  <Lottie
                    ref={this.handleAnimationRef}
                    autoPlay={false}
                    resizeMode="cover"
                    loop={animation.loop}
                    style={{ width: scale(60), height: scale(60) }}
                    source={animation.source}
                    autoSize={true}
                  />
                </View>
                <Space height={scale(24)} />
                {title ? (
                  <Fragment>
                    <Text style={styles.title}>{title}</Text>
                  </Fragment>
                ) : null}
                {message ? (
                  <Fragment>
                    <Space height={scale(12)} />
                    <Text style={[textStyles.body2, { marginHorizontal: scale(28), textAlign: 'center' }]}>{message}</Text>
                  </Fragment>
                ) : null}
                {this.renderDetail()}
              </View>
              {this.renderActions()}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

export default AnimationAlertModal;

AnimationAlertModal.propTypes = {
  visible: PropTypes.bool,
  animationType: PropTypes.oneOf(['loading', 'success', 'error']),
  message: PropTypes.string,
};

AnimationAlertModal.defaultProps = {
  visible: false,
  animationType: 'loading',
  onPress: null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: scale(40),
    backgroundColor: colors.black60,
  },
  detailContainer: {
    flexDirection: 'row',
    maxHeight: screen.height / 2,
    marginTop: screen.padding.small,
    paddingTop: screen.padding.small,
    paddingHorizontal: screen.padding.small,
    borderTopColor: colors.lightGray,
    borderTopWidth: 1,
  },
  body: {
    alignItems: 'center',
    paddingVertical: scale(32),
  },
  title: {
    textAlign: 'center',
    marginHorizontal: scale(48),
    fontFamily: 'sale-text-semibold',
    fontSize: scale(17),
  },
});
