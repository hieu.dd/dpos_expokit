import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

import { scale } from '../../utils/scaling';
import { colors, screen, textStyles } from '../../resources/styles/common';

/**
 * @augments {Component<{  title:string>}
 */
class ListHeader extends PureComponent {
  render() {
    const { title } = this.props;

    return (
      <View style={styles.container}>
        <Text style={textStyles.footnote}>{title}</Text>
      </View>
    );
  }
}

export default ListHeader;

ListHeader.propTypes = {
  title: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    height: scale(32),
    justifyContent: 'center',
    paddingLeft: screen.padding.smaller,
    backgroundColor: colors.lightGray,
  },
});
