import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { scale } from '../../utils/scaling';
import { colors } from '../../resources/styles/common';

/**
 * @augments {Component<{  checked:boolean,  disabled:boolean,  size:number,  containerStyle: View.propTypes.style,  onPress:Function>}
 */
class CheckButton extends PureComponent {
  render() {
    const { checked, disabled, onPress, size, containerStyle } = this.props;

    return (
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <View style={[styles.container, containerStyle]}>
          <Icon
            name={checked ? 'check-circle' : 'radio-button-unchecked'}
            type="material"
            color={checked ? colors.secondary : colors.gray}
            size={size}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

export default CheckButton;

CheckButton.propTypes = {
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  size: PropTypes.number,
  containerStyle: View.propTypes.style,
  onPress: PropTypes.func,
};

CheckButton.defaultProps = {
  checked: false,
  disabled: false,
  size: scale(20),
};

const styles = StyleSheet.create({
  container: {
    width: scale(16),
    height: scale(16),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
