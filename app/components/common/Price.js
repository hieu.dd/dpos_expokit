import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';
import { Util } from 'teko-js-sale-library';

import { colors, textStyles } from '../../resources/styles/common';
import { scale, getLetterSpacing } from '../../utils/scaling';

export class Price extends PureComponent {
  render() {
    const { price, unit, color, style } = this.props;

    return (
      <View style={styles.container}>
        <Text style={[textStyles.heading1, styles.price, style, { color }]}>{`${Util.Text.numberWithCommas(
          price
        )}\u20ab`}</Text>
        {/* <Text style={[styles.unit, { color }]}>{unit}</Text> */}
      </View>
    );
  }
}

export default Price;

Price.propTypes = {
  price: PropTypes.number,
  unit: PropTypes.string,
  color: PropTypes.string,
};

Price.defaultProps = {
  price: 0,
  unit: 'đ',
  color: colors.primary,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  price: {
    color: colors.primary,
  },
  unit: {
    fontSize: scale(12),
    paddingBottom: scale(6),
    color: colors.primary,
    letterSpacing: getLetterSpacing(-0.4),
    fontFamily: 'sale-text-semibold',
  },
});
