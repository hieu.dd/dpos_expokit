import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { screen, colors } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';

export default class TrapezoidButton extends PureComponent {
  render() {
    const {
      onPress,
      title,
      disabled,
      color,
      iconRightName,
      iconLeftName,
      iconType,
      iconSize,
      iconColor,
      textStyle,
      width,
      height,
      left,
      right,
    } = this.props;

    let trapezoid = {
      width,
      height: 0,
      borderBottomWidth: height,
      borderBottomColor: disabled ? '#e1e1e1' : color,
      borderLeftWidth: left,
      borderLeftColor: 'transparent',
      borderRightWidth: right,
      borderRightColor: 'transparent',
      borderStyle: 'solid',
    };

    return (
      <View
        style={{
          width,
          height,
        }}
      >
        <View style={trapezoid} />
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            paddingLeft: left,
            paddingRight: right,
          }}
        >
          <TouchableOpacity onPress={onPress} style={styles.content} disabled={disabled}>
            {iconLeftName ? (
              <Icon
                type={iconType}
                name={iconLeftName}
                color={disabled ? colors.darkGray : iconColor}
                size={iconSize}
                style={styles.icon}
              />
            ) : null}
            {title ? <Text style={[styles.title, textStyle, disabled ? { color: colors.darkGray } : {}]}>{title}</Text> : null}
            {iconRightName ? (
              <Icon
                type={iconType}
                name={iconRightName}
                color={disabled ? colors.darkGray : iconColor}
                size={iconSize}
                style={styles.icon}
              />
            ) : null}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

TrapezoidButton.propTypes = {
  title: PropTypes.string,
  color: PropTypes.string,
  iconRightName: PropTypes.string,
  iconLeftName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.number,
  onPress: PropTypes.func,
  textStyle: Text.propTypes.style,
};

TrapezoidButton.defaultProps = {
  iconType: 'material-community',
  iconSize: screen.iconSize.default,
  color: '#4285f4',
  containerStyle: {},
  left: 0,
  right: 0,
  height: 50,
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: screen.padding.default,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    // marginLeft: screen.margin.default,
    // marginRight: screen.margin.default,
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: screen.fontSize.large,
    marginHorizontal: scale(4),
  },
});
