import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

import { screen } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';

export default class Button extends PureComponent {
  render() {
    const { onPress, title, disabled, color, iconName, iconType, iconSize, iconColor, containerStyle, textStyle } = this.props;

    return (
      <View style={[styles.container, containerStyle, { backgroundColor: color }]}>
        <TouchableOpacity onPress={onPress} style={styles.content} disabled={disabled}>
          {iconName ? <Icon type={iconType} name={iconName} color={iconColor} size={iconSize} style={styles.icon} /> : null}
          {title ? <Text style={[styles.title, textStyle]}>{title}</Text> : null}
        </TouchableOpacity>
      </View>
    );
  }
}

Button.propTypes = {
  title: PropTypes.string,
  color: PropTypes.string,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.number,
  containerStyle: View.propTypes.style,
  onPress: PropTypes.func,
  textStyle: Text.propTypes.style,
};

Button.defaultProps = {
  iconType: 'material-community',
  iconSize: screen.iconSize.default,
  color: '#4285f4',
  containerStyle: {},
};

const styles = StyleSheet.create({
  container: {
    height: scale(40),
    flexDirection: 'row',
    borderRadius: scale(8),
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: screen.padding.default,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    marginLeft: screen.margin.default,
    marginRight: screen.margin.default,
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: screen.fontSize.large,
    marginLeft: screen.margin.default,
    marginRight: screen.margin.default,
  },
});
