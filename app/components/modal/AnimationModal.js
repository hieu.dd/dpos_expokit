import { connect } from 'react-redux';

import { hideAnimationModal } from '../../stores/appState/actions';
import AnimationAlertModal from '../alert/AnimationAlertModal';

function mapStateToProps(state) {
  return {
    ...state.appState.animationModal,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onClose: () => dispatch(hideAnimationModal()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AnimationAlertModal);
