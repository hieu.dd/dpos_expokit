import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, View, StatusBar, Text, ScrollView, StyleSheet } from 'react-native';

import * as appStateActions from '../../stores/appState/actions';
import { scale } from '../../utils/scaling';
import commonStyles, { screen, colors, textStyles } from '../../resources/styles/common';
import ModalButtons from './ModalButtons';
import LoadingIndicator from '../common/LoadingIndicator';

const defaultActions = [{ text: 'Đóng' }];

export class ProcessingPrompt extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    if (!nextProps.visible) {
      return { showDetail: false };
    }

    return null;
  }

  state = {
    showDetail: false,
  };

  constructor(props) {
    super(props);
  }

  onRequestClose = () => null;

  onClose = () => {
    this.props.closeLoadingModal();
  };

  renderLoadingBody = () => {
    const { loadingText } = this.props;

    return (
      <View style={commonStyles.rowAndCenter}>
        <View style={styles.loadingContainer}>
          <LoadingIndicator />
          {loadingText.trim() ? (
            <Text style={[textStyles.heading1, { paddingLeft: scale(8) }]}>{loadingText.trim()}</Text>
          ) : null}
        </View>
      </View>
    );
  };

  renderInfoBody = () => {
    const { message, title } = this.props;

    return (
      <View style={styles.modalBody}>
        <View style={styles.header}>
          {title ? <Text style={styles.title}>{title}</Text> : null}
          <Text style={[textStyles.body1, styles.label]}>{message}</Text>
          {this.renderDetail()}
        </View>
        {this.renderActions()}
      </View>
    );
  };

  renderDetail = () => {
    const { detail } = this.props;

    if (!detail || !this.state.showDetail) return null;

    return (
      <View style={styles.detailContainer}>
        <ScrollView style={{ maxHeight: screen.height / 2 }}>
          <Text>{detail}</Text>
        </ScrollView>
      </View>
    );
  };

  onToggleDetail = () => {
    this.setState({ showDetail: !this.state.showDetail });
  };

  renderActions = () => {
    let { actions } = this.props;
    if (!actions || actions.length === 0) {
      actions = defaultActions;
    }
    actions = this.addShowDetailAction(actions);
    actions = actions.map(action => ({
      ...action,
      onPress: () => {
        !action.shouldNotClose && this.onClose();
        action.onPress && action.onPress();
      },
    }));

    return <ModalButtons actions={actions} />;
  };

  addShowDetailAction = actions => {
    if (this.props.detail) {
      actions = [
        { text: this.state.showDetail ? 'Ẩn' : 'Chi tiết', onPress: this.onToggleDetail, shouldNotClose: true },
        ...actions,
      ];
    }

    return actions;
  };

  render() {
    let { visible, loadingText, backgroundColor } = this.props;

    return (
      <Modal visible={visible} onRequestClose={this.onRequestClose} animationType="none" transparent={true}>
        <View style={[styles.modalContainer, { backgroundColor }]}>
          <StatusBar hidden={true} />

          {loadingText ? this.renderLoadingBody() : this.renderInfoBody()}
        </View>
      </Modal>
    );
  }
}

ProcessingPrompt.propTypes = {
  closeLoadingModal: PropTypes.func,
  actions: PropTypes.array,
  title: PropTypes.string,
  loadingText: PropTypes.string,
  message: PropTypes.string,
  backgroundColor: PropTypes.string,
  visible: PropTypes.bool,
};

ProcessingPrompt.defaultProps = {
  actions: [],
};

ProcessingPrompt.defaultProps = {
  backgroundColor: colors.black60,
};

function mapStateToProps(state) {
  return {
    ...state.appState.loadingModal,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeLoadingModal: () => dispatch(appStateActions.hideLoadingModal()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProcessingPrompt);

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  modalBody: {
    borderRadius: scale(18),
    marginHorizontal: scale(32),
    backgroundColor: 'white',
  },
  header: {
    paddingVertical: scale(24),
    paddingHorizontal: scale(32),
  },
  loadingContainer: {
    padding: screen.padding.default,
    backgroundColor: 'white',
    borderRadius: scale(8),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: screen.fontSize.larger,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: screen.margin.default,
  },
  detailContainer: {
    marginTop: screen.margin.small,
    paddingTop: screen.padding.small,
    borderTopColor: colors.lightGray,
    borderTopWidth: 1,
  },
  label: {
    textAlign: 'center',
  },
});
