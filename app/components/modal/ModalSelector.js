import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TextInput, TouchableOpacity, Modal } from 'react-native';

import { screen, colors } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import ModalHeader from './ModalHeader';
import { FlatList } from 'react-native-gesture-handler';
import { SearchBar, Icon } from 'react-native-elements';
import { Util } from 'teko-js-sale-library';
import SearchComponent from '../header/SearchComponent';

class ModalSelector extends PureComponent {
  state = {
    modalVisible: false,
    filter: '',
  };

  onRequestClose = () => {};

  onChange = item => {
    this.props.onChange(item);
    this.onPress();
  };

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={{
          paddingVertical: scale(15),
          borderBottomColor: colors.lightGray,
          borderBottomWidth: 1,
        }}
        onPress={() => this.onChange(item)}
      >
        <Text style={{ fontSize: scale(15), fontFamily: 'sale-text-regular' }}>{item.label}</Text>
      </TouchableOpacity>
    );
  };

  onPress = () => this.setState({ modalVisible: !this.state.modalVisible });

  onChangeText = filter => {
    this.setState({ filter });
  };

  onClear = () => {};

  render() {
    let { selectTextStyle, style, overlayColor, data, placeholder, children, cancelText, ...rest } = this.props;
    let { filter } = this.state;
    if (filter) {
      data = data.filter(item =>
        Util.Text.convertVNstring(item.label)
          .toLowerCase()
          .includes(Util.Text.convertVNstring(filter.trim()).toLowerCase())
      );
    }
    return (
      <View style={style}>
        <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={this.onPress}>
          <Text style={selectTextStyle}>{this.props.value}</Text>
        </TouchableOpacity>
        <Modal
          {...rest}
          visible={this.state.modalVisible}
          animationType="slide"
          transparent={true}
          onRequestClose={this.onRequestClose}
        >
          <View style={{ flex: 1, backgroundColor: colors.black60, paddingTop: screen.header.headerHeight }}>
            <ModalHeader title={this.props.title} leftButton={{ text: cancelText, onPress: this.onPress }} />
            <SearchComponent
              onEndEditing={this.onLoadOrders}
              placeholder={placeholder}
              colors={['white', 'white']}
              onChangeText={this.onChangeText}
              onClear={this.onClear}
              iconColor={colors.darkGray}
              inputContainerStyle={{ backgroundColor: colors.lightGray, height: scale(40) }}
              containerStyle={{ paddingTop: screen.padding.smaller, paddingBottom: screen.padding.smaller, height: -1 }}
              placeholderTextColor={colors.darkGray}
            />
            <View style={{ flex: 1, backgroundColor: 'white', paddingHorizontal: screen.padding.smaller }}>
              <FlatList
                keyboardShouldPersistTaps={'handled'}
                renderItem={this.renderItem}
                data={data}
                keyExtractor={item => item.key}
              />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default ModalSelector;

ModalSelector.propTypes = {
  cancelText: PropTypes.string,
};

ModalSelector.defaultProps = {
  cancelText: 'Đóng',
};
