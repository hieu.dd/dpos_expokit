import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Util } from 'teko-js-sale-library';
import PromoManager from 'teko-promotion-parser';

import { getNow } from '../../modules/promotion';
import * as comboUtils from '../../modules/promotion/combo';

function sortCombo(c1, c2) {
  let deltaPriority = c2.priority - c1.priority;
  if (deltaPriority === 0) {
    return c2.combo.total_value - c1.combo.total_value;
  } else {
    return deltaPriority;
  }
}

export default function withActiveCombos(WrappedComponent) {
  class WithActiveCombosComponent extends Component {
    state = {
      allActiveCombosWithSuggestion: [],
    };

    onPromoNewChange = () => {
      this.checkCombos();
    };

    componentDidMount() {
      this.checkCombos();
      this.unsubscribeOnChangePromoNew = PromoManager.addOnChangeObjectListener(this.onPromoNewChange);
    }

    componentWillUnmount() {
      this.unsubscribeOnChangePromoNew && this.unsubscribeOnChangePromoNew();
    }

    checkCombos() {
      let { currentCart, products } = this.props;

      let allActiveCombos = PromoManager.getAllActiveCombos(null, getNow(), 'agent');
      let productsToCheckCombo = [] || products || currentCart.items;
      let allActiveCombosWithSuggestion = [];

      if (productsToCheckCombo.length > 0 && allActiveCombos.length > 0) {
        allActiveCombosWithSuggestion = allActiveCombos
          .map(combo => {
            let suggestProducts = comboUtils.suggestProductsInCartForCombo(productsToCheckCombo, combo);
            let comboSize = comboUtils.getComboSize(combo);
            let completeRate = suggestProducts.length / comboSize;
            let isComboNew = comboUtils.checkComboNew(combo);
            let isValidCombo =
              comboUtils.isValidComboValue(combo, suggestProducts, []) && (isComboNew || completeRate === 1) ? 2 : 0;
            let priority = isValidCombo ? 2 : 0 + completeRate;

            return {
              combo,
              isComboNew,
              suggestProducts,
              comboSize,
              isValidCombo,
              priority,
            };
          })
          .sort(sortCombo);
      } else {
        allActiveCombosWithSuggestion = allActiveCombos.map(combo => ({
          combo,
          isComboNew: comboUtils.checkComboNew(combo),
          suggestProducts: [],
          comboSize: comboUtils.getComboSize(combo),
          completeRate: 0,
        }));
      }

      allActiveCombosWithSuggestion = allActiveCombosWithSuggestion.map(item => ({
        ...item,
        searchedName: typeof item.combo.name === 'string' ? Util.Text.convertVNstring(item.combo.name).toLocaleLowerCase() : '',
        searchedDescription:
          typeof item.combo.description === 'string'
            ? Util.Text.convertVNstring(item.combo.description).toLocaleLowerCase()
            : '',
        banner_url: this.props.promotionsNew[item.combo.programKey].banner_url,
      }));

      this.setState({ allActiveCombosWithSuggestion });

      if (this.props.onHasAvailableCombos) {
        this.checkAvailableCombos(allActiveCombosWithSuggestion);
      }
    }

    checkAvailableCombos = activeCombos => {
      let { filterCombo } = this.props;
      let availableCombos = activeCombos.filter(filterCombo);
      let status = availableCombos.length > 0;

      this.props.onHasAvailableCombos(status);
    };

    render = () => {
      return <WrappedComponent {...this.props} activeCombos={this.state.allActiveCombosWithSuggestion} />;
    };
  }

  function mapStateToProps(state) {
    return {
      promotionsNew: state.firebase.promotionsNew,
    };
  }

  return connect(mapStateToProps)(WithActiveCombosComponent);
}
