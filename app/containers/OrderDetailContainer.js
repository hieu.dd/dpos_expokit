import React, { Component } from 'react';
import { View, BackHandler, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import * as saleActions from '../stores/sale/actions';
import * as cartActions from '../stores/cart/actions';
import { Icon } from 'react-native-elements';
import { colors, screen, textStyles } from '../resources/styles/common';
import { scale } from '../utils/scaling';
import CustomerInformation from '../components/CustomerInformation';
import ChoosePaymentComponent from '../components/payment/ChoosePaymentComponent';
import * as cartUtils from '../utils/cart';
import OrderResultComponent from '../components/order/OrderResultComponent';

class OrderDetailContainer extends Component {
  state = {
    step: 1,
    createOrderStatus: 'loading',
    finalInvoice: 0,
    paymentStatus: '',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    let { cartState, currentTransaction, payment } = nextProps;
    let paymentStatus =
      currentTransaction &&
      currentTransaction.id &&
      payment &&
      payment[currentTransaction.id] &&
      payment[currentTransaction.id][0] &&
      payment[currentTransaction.id][0].status;
    let currentCart = cartUtils.getCurrentCart(cartState);
    let totalCartDiscount = cartUtils.getTotalCartDiscount(currentCart);
    let finalInvoice = Math.max(currentCart.totalInvoice - totalCartDiscount, 0);

    let derivedState = { totalCartDiscount, finalInvoice, paymentStatus };
    if (paymentStatus) {
      derivedState.step = 3;
    }
    return derivedState;
  }

  componentWillUnmount() {
    this.props.clearPayment();
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
  }

  onBackPressed = () => {
    let { step } = this.state;
    if (step === 1) {
      this.props.navigation.goBack();
    } else if (step === 2) {
      this.setState({ step: 1 });
    }
    return true;
  };

  renderStepButton = (index, style, isCurrentStep) => {
    return (
      <View
        style={[
          styles.stepButton,
          style,
          { backgroundColor: isCurrentStep ? colors.primary : 'white', borderWidth: isCurrentStep ? 0 : 1 },
        ]}
      >
        <Text style={[textStyles.footnote, { color: isCurrentStep ? 'white' : colors.darkGray }]}>{index}</Text>
      </View>
    );
  };

  renderStepText = (text, isCurrentStep) => {
    return (
      <View style={{ flex: 1, alignItems: 'center', paddingTop: scale(12) }}>
        <Text style={[textStyles.footnote, { color: isCurrentStep ? colors.primary : colors.darkGray }]}>{text}</Text>
      </View>
    );
  };

  onCreateOrder = () => {
    this.setState({
      step: 3,
    });
  };

  renderButtonStep = (iconName, style, isCurrentStep) => {
    return (
      <View
        style={[
          {
            borderRadius: scale(18),
            width: scale(36),
            height: scale(36),
            backgroundColor: isCurrentStep ? colors.dark_slate_blue : 'white',
            alignItems: 'center',
            justifyContent: 'center',
          },
          style,
        ]}
      >
        <Icon size={scale(16)} name={iconName} type={'material-community'} color={isCurrentStep ? 'white' : colors.primary} />
      </View>
    );
  };

  renderStepBar = () => {
    let { step } = this.state;
    return (
      <View
        style={{
          height: scale(36),
          width: screen.width,
          flexDirection: 'row',
          marginTop: scale(-18),
        }}
      >
        {this.renderButtonStep('account', { marginLeft: scale(70) }, step >= 1)}
        {this.renderButtonStep('credit-card', { marginLeft: scale(56) }, step >= 2)}
        {this.renderButtonStep('check-all', { marginLeft: scale(56) }, step >= 3)}
      </View>
    );
  };

  onCreateSuccess = () => {
    this.setState({
      createOrderStatus: 'success',
    });
  };

  onCreateFailure = () => {
    this.setState({
      createOrderStatus: 'error',
    });
  };

  onCancel = () => {
    if (this.state.step === 1) {
      this.props.navigation.goBack();
    } else if (this.state.step === 2) {
      this.setState({
        step: 1,
      });
    }
  };

  onContinue = () => {
    this.setState({ step: 2 });
  };

  onRetry = () => {
    this.setState({ step: 2 });
  };

  render() {
    let { step, paymentStatus, createOrderStatus } = this.state;
    let { currentTransaction } = this.props;
    let { finalInvoice } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: colors.lightGray }}>
        {/* <CustomStatusBar /> */}
        <View style={{ alignItems: 'center', height: scale(92), backgroundColor: colors.primary }}>
          <Text style={[textStyles.small, { color: colors.lightGold, marginTop: scale(16) }]}>BƯỚC {step}/3</Text>
          <Text style={[textStyles.fontBig, { color: 'white' }]}>
            {step === 1 ? 'Thông tin' : step === 2 ? 'Thanh toán' : 'Xác nhận'}
          </Text>
        </View>
        {this.renderStepBar()}
        <View style={{ flex: 1 }}>
          {step === 1 && !paymentStatus ? (
            <CustomerInformation
              finalInvoice={finalInvoice || (currentTransaction.invoice && currentTransaction.invoice.price)}
              onContinue={this.onContinue}
              navigation={this.props.navigation}
            />
          ) : null}
          {step === 2 && !paymentStatus ? (
            <ChoosePaymentComponent
              createOrder={this.onCreateOrder}
              onSuccess={this.onCreateSuccess}
              onFailure={this.onCreateFailure}
              finalInvoice={finalInvoice || (currentTransaction.invoice && currentTransaction.invoice.price)}
            />
          ) : null}
          {step === 3 || paymentStatus ? (
            <OrderResultComponent
              paymentStatus={paymentStatus}
              createOrderStatus={createOrderStatus}
              transaction={currentTransaction}
              navigation={this.props.navigation}
              onRetry={this.onRetry}
            />
          ) : null}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    cartState: state.cart,
    payment: state.firebase.payment,
    currentTransaction: state.sale.currentTransaction,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveCustomerInfo: customerInfo => dispatch(cartActions.saveCustomerInfo(customerInfo)),
    clearPayment: () => dispatch(saleActions.clearPayment()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetailContainer);

const styles = StyleSheet.create({
  stepButton: {
    position: 'absolute',
    top: scale(34),
    width: scale(24),
    height: scale(24),
    borderRadius: scale(12),
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: colors.darkGray,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
