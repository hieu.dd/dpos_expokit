import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import * as saleActions from '../stores/sale/actions';
import * as cartActions from '../stores/cart/actions';
import * as customerUtils from '../utils/customer';
import moment from 'moment';

class CustomerInformationContainer extends Component {
  state = {
    customer_phone: '0943310394',
    customer_name: 'Hiếu',
    receiver_phone: '0943310394',
    receiver_name: 'Hiếu',
    receiver_province: 'Hà Nội',
    receiver_province_code: '01',
    receiver_district: 'Hà Đông',
    receiver_district_code: '0108',
    receiver_street: 'ffff',
    receiver_date: moment()
      .add(moment().weekday() === 4 ? 4 : 3, 'd')
      .format('DD/MM/YYYY'),
    receiver_time: moment().format('hh:mm A'),
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity
          style={{ margin: 100, backgroundColor: 'red' }}
          onPress={() => {
            let customerInfo = customerUtils.mapCustomerInfoFromState(this.state);
            this.props.saveCustomerInfo(customerInfo);
            this.props.sendOrderToBE({});
          }}
        >
          <Text>11212121212121</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    saveCustomerInfo: customerInfo => dispatch(cartActions.saveCustomerInfo(customerInfo)),

    sendOrderToBE: ({ onSuccess, onFailure }) => dispatch(saleActions.sendOrderToBE({ onSuccess, onFailure })),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerInformationContainer);
