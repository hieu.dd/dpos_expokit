import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions, Image } from 'react-native';
import { connect } from 'react-redux';

import ComboList from '../components/combo/ComboList';
import SwipeComboList from '../components/combo/SwipeComboList';
import { fetchCategories } from '../stores/product/actions';

import withActiveCombos from './combo/withActiveCombos';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

const WrappedComboList = withActiveCombos(SwipeComboList);

export class HomeContainer extends Component {
  componentDidMount() {
    this.props.fetchCategories();
  }

  onPressComboItem = item => {
    this.props.navigation.navigate('Combo', {
      combo: item.combo,
      cartRouteName: 'Cart',
    });
  };

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <WrappedComboList
          onPressComboItem={this.onPressComboItem}
          searchText={''}
          shouldShowListHeader={false}
          showBanner={true}
          navigation={this.props.navigation}
          promotionsOld={this.props.promotionsOld}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    promotionsOld: state.firebase.promotionsOld,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchCategories: () => dispatch(fetchCategories()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer);

const styles = StyleSheet.create({});
