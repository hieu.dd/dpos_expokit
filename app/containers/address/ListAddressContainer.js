import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Switch } from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import * as strings from '../../resources/strings';
import { colors, screen, textStyles } from '../../resources/styles/common';
import { scale } from '../../utils/scaling';
import { TextInput, FlatList, ScrollView } from 'react-native-gesture-handler';
import Space from '../../components/Space';
import Button from '../../components/button/Button';
import * as customerUtils from '../../utils/customer';
import { connect } from 'react-redux';
import * as saleActions from '../../stores/sale/actions';
import * as cartActions from '../../stores/cart/actions';
import * as customerActions from '../../stores/customer/actions';
import { Util } from 'teko-js-sale-library';
import InputRow from '../../components/InputRow';
import KeyboardTextInput from '../../components/modal/KeyboardTextInput';
import Header from '../../components/header/Header';
import HeaderIcon from '../../components/header/HeaderIcon';
import TrapezoidButton from '../../components/button/TrapezoidButton';

export class NewAddressContainer extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    let indexContact = nextProps.navigation.state.params.index;
    return { indexContact };
  }

  onGoBack = () => {
    this.props.navigation.goBack();
  };

  renderLeftSection = () => {
    return (
      <TrapezoidButton
        color={colors.lightGold}
        iconSize={scale(24)}
        // title={'QUAY LẠI'}
        // textStyle={[textStyles.small, { fontFamily: 'sale-text-bold', color: 'black' }]}
        width={scale(70)}
        height={scale(50)}
        right={scale(12)}
        iconLeftName="chevron-left"
        iconType="material"
        iconColor="black"
        onPress={this.onGoBack}
      />
    );
  };

  renderHeader = () => {
    return (
      <Header
        gradient={false}
        color={colors.primary}
        titleTextStyle={[textStyles.price, { color: 'white' }]}
        title={'Địa chỉ nhận hàng'}
        leftSection={this.renderLeftSection()}
      />
    );
  };

  onAddNewAddress = () => {
    let { customer_id } = this.props.navigation.state.params;
    this.props.navigation.navigate('NewAddress', { customer_id });
  };

  onChangeChoosedAddress = (item = null, index = -1) => {
    let { chooseContact } = this.props.navigation.state.params;
    chooseContact(item, index);
    this.props.navigation.goBack();
  };

  renderContactItem = ({ item, index }) => {
    let { indexContact } = this.state;
    let checked = indexContact === index;
    return (
      <TouchableOpacity
        onPress={() => this.onChangeChoosedAddress(item, index)}
        style={[styles.item, checked ? styles.checked : {}]}
      >
        <Icon name="map-marker" type="material-community" size={scale(16)} color={colors.primary} />
        <Text style={[textStyles.body1, styles.textItem]}>{customerUtils.getFullAddressOfContact(item)}</Text>
        {checked ? (
          <Icon name="check-circle" type="material-community" size={scale(16)} color={colors.secondary} />
        ) : (
          <View style={{ width: scale(24), height: scale(24) }} />
        )}
      </TouchableOpacity>
    );
  };

  renderListAddress = () => {
    let { customer_id } = this.props.navigation.state.params;
    let { customer } = this.props;
    let contacts = customer.searchResults.find(customer => customer_id === customer.id).contacts;
    return <FlatList data={contacts} renderItem={this.renderContactItem} />;
  };

  render() {
    let { indexContact } = this.state;
    let receiveInShowroom = indexContact === -1;
    return (
      <View style={{ flex: 1, backgroundColor: colors.lightGray }}>
        {this.renderHeader()}
        <ScrollView style={{ padding: scale(10) }}>
          {this.renderListAddress()}
          <View style={styles.space} />
          <TouchableOpacity onPress={this.onChangeChoosedAddress} style={styles.item}>
            <Icon name="map-marker" type="material-community" size={scale(16)} color={colors.primary} />
            <Text style={[textStyles.body1, styles.textItem]}>Nhận hàng tại Showroom</Text>
            {receiveInShowroom ? (
              <Icon name="check" type="material-community" size={scale(24)} color={colors.primary} />
            ) : (
              <View style={{ width: scale(24), height: scale(24) }} />
            )}
          </TouchableOpacity>
          <View style={styles.space} />
          <TouchableOpacity onPress={this.onAddNewAddress} style={[styles.item, { alignItems: 'center' }]}>
            <Text style={[textStyles.body1, { color: colors.primary, flex: 1, textAlignVertical: 'center' }]}>
              THÊM ĐỊA CHỈ MỚI
            </Text>
            <Icon name="add" type="material" size={scale(16)} color={colors.primary} />
          </TouchableOpacity>
          <View style={[styles.space, { height: scale(50) }]} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    customer: state.customer,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewAddressContainer);

const styles = StyleSheet.create({
  space: {
    backgroundColor: colors.lightGray,
    height: scale(10),
  },
  item: {
    padding: scale(12),
    flexDirection: 'row',
    borderTopWidth: scale(1),
    backgroundColor: 'white',
    borderTopColor: colors.lightGray,
    minHeight: scale(44),
  },
  textItem: { flex: 1, textAlignVertical: 'center', marginHorizontal: scale(12) },
  checked: {
    borderTopWidth: scale(1),
    borderTopColor: colors.secondary,
    borderColor: colors.secondary,
    borderWidth: scale(1),
  },
});
