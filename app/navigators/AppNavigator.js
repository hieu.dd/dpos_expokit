import React from 'react';
import { AppState, BackHandler, NetInfo, View, Animated, Text, Alert, Image, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';

import RootNavigationService from '../services/navigation/RootNavigationService';

import { listenPromotions } from '../stores/firebase/actions';
import { changeAppState, changeConnectionStatus, initData, syncFirebaseConfig } from '../stores/appState/actions';
import { colors, screen } from '../resources/styles/common';
import ComboContainer from '../containers/combo/ComboContainer';
import HomeContainer from '../containers/HomeContainer';
import CustomerInformationContainer from '../containers/CustomerInfomationContainer';
import OrderDetailContainer from '../containers/OrderDetailContainer';
import NewAddressContainer from '../containers/address/NewAddressContainer';
import MapContainer from '../containers/address/MapContainer';
import ProductDetailContainer from '../containers/product/ProductDetailContainer';

import ListAddressContainer from '../containers/address/ListAddressContainer';
import ComboDetailContainer from '../containers/combo/ComboDetailContainer';
// MAIN TAB NAVIGATOR
import MainTabNavigator from './MainTabNavigator';

const AppNavigator = createStackNavigator(
  {
    Main: HomeContainer,
    Combo: { screen: ComboContainer },
    CustomerInformation: { screen: CustomerInformationContainer },
    OrderDetail: { screen: OrderDetailContainer },
    NewAddress: { screen: NewAddressContainer },
    ListAddress: { screen: ListAddressContainer },
    ComboDetail: { screen: ComboDetailContainer },
    Map: { screen: MapContainer },
    ProductDetail: {
      screen: ProductDetailContainer,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Main',
    navigationOptions: {
      headerTitleStyle: {
        textAlign: 'center',
        flex: 1,
      },
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: screen.headerColor,
      },
    },
  }
);

class AppNavigatorWithState extends React.Component {
  state = {
    splashAnimation: new Animated.Value(0),
  };

  onBackPressed() {
    return false;
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this.onConnectionChange);
    BackHandler.addEventListener('hardwareBackPress', this.onBackPressed);
    AppState.addEventListener('change', this.onAppStateChange);

    this.props.initData();
    this.props.listenPromotions();
    this.props.syncFirebaseConfig();
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.onConnectionChange);
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPressed);
    AppState.removeEventListener('change', this.onAppStateChange);
  }

  onAppStateChange = nextAppState => {
    if (this.props.appState.app.state) {
      if (this.props.appState.app.state.match(/inactive|background/) && nextAppState === 'active') {
        // app resume
      } else if (this.props.appState.app.state === 'active' && nextAppState.match(/inactive|background/)) {
        // app pause
      }
    }
    this.props.changeAppState(nextAppState);
  };

  onConnectionChange = isConnected => {
    this.props.changeConnectionStatus(isConnected);
  };

  showAlertConnect() {
    Alert.alert(
      '',
      `Ứng dụng bị mất kết nối, vui lòng kiểm tra lại kết nối internet`,
      [
        {
          text: 'Thử lại',
          onPress: () => {
            !this.props.appState.network.isConnected ? this.showAlertConnect() : null;
          },
        },
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppNavigator
          ref={navigatorRef => {
            RootNavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  appState: state.appState,
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  initData: () => dispatch(initData()),
  syncFirebaseConfig: () => dispatch(syncFirebaseConfig()),
  listenPromotions: () => dispatch(listenPromotions()),
  changeConnectionStatus: isConnected => dispatch(changeConnectionStatus(isConnected)),
  changeAppState: nextAppState => dispatch(changeAppState(nextAppState)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppNavigatorWithState);
